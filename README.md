# Eve #

A JavaScript library containing multiple String, Collection, Object utilities.

### Features ###
* Clean, simple API
* Useful, non-trivial functions (not just syntactic sugar)
* Fast and light-weight

### Set Up ###

1. Download the latest release (as of 13 Nov 2019, v3.0.0).
2. Include in your HTML (preferably just before `</body>`):

    `<script src="eve-3.0.0.min.js"></script>`

### Browser Support ###
* Chrome
* Edge
* Firefox
* Opera
* Safari

### API ###
**`escapeHTML / htmlspecialchars`**

```
/***
 * Makes string safe for printing, i.e. contains no characters that may be parsed as HTML syntax, e.g.
 * '&' (ampersand) becomes '&amp;' 
 * '<' (less than) becomes '&lt;'
 * @param {string}
 * @return {string}
 ***/

// Sample usage
var safeString = eve.escapeHTML('<a href="http://evil.com">');  // &lt;a href=&quot;http:&#x2F;&#x2F;evil.com&quot;&gt;
```

**`unescapeHTML / htmlspecialchars_decode`**
```
/***
 * Inverse function of escapeHTML() / htmlspecialchars()
 * @param {string}
 * @return {string}
 ***/
 
// Sample usage
var safeString = '&lt;p&gt;I am an HTML paragraph.&lt;/p&gt;';
var html = eve.unescapeHTML(safeString);  // <p>I am an HTML paragraph.</p>
```

**`encodeURL / urlencode`**
```
/***
 * Encodes URL characters, e.g. spaces become %20, quotes become %22 etc.
 * Adheres to RFC-3986.
 * @param {string}
 * @return {string}
 ***/

// Sample usage
var encoded = eve.encodeURL('I love URLs');  // I%20love%20URLs
```

**`decodeURL / urldecode`**
```
/***
 * Decodes URL characters, e.g. %20 become spaces, %22 become quotes etc.
 * Adheres to RFC-3986.
 * @param {string}
 * @return {string}
 ***/

// Sample usage
var decoded = eve.decodeURL('I%20love%20URLs');  // I love URLs
```

**`br2nl`**
```
/***
 * Changes all <br> and <br /> tags to newline "\n"
 * @param {string}
 * @return {string}
 ***/
 
// Sample usage
var text_from_html = eve.br2nl('Line 1<br>Line 2');
```

**`nl2br`**
```
/***
 * Changes all newlines ["\r", "\n", \r\n"] to <br> tags
 * @param {string}
 * @return {string}
 ***/
 
// Sample usage
var text = "Line 1\n" +
           "Line 2\r" +
		   "Line 3\r\n";
var html_from_text = eve.nl2br(text); // Line 1<br>Line 2<br>Line 3<br>
```

**`utf82ascii`**
```
/***
 * Converts UTF-8 strings to ASCII by translating non-ASCII characters to the closest counterpart.
 * @param {string}
 * @return (string)
 ***/
 
// Sample usage
var utf8_text = "My doppelgänger fiancé's raison d'être is the Führer exposé of the El Niño entrepôt."
     // we want "My doppelganger fiance's raison d'etre is the Fuhrer expose of the El Nino entrepot."
var ascii_text = eve.utf82ascii(utf8_text);
```

**`slugify`**
```
/***
 * Converts a string to a slug, e.g. "Hello World" ==> "hello-world"
 * - Note: output is a lower-case ASCII string
 * @param: {string}
 * @return: {string}
 ***/
 
// Sample usage
var article_title = "Hello World";
var article_url = 'http://www.example.com/' + eve.slugify(article_title);
```

**`csv2multiarrays`**
```
/**
 * Parse a CSV string into a 2D array of strings.
 * conform to RFC-4180, which allows multiple records (each a CSV)
 * @param: {string} data,
 *         {object} options (optional)
 *                  - {string} delimiter (defaults to comma. Must be 1 char)
 *                  - {string} lineEnding (defaults to \n. Max of 2 chars)
 *                  - {string} quote {defaults to ". Must be 1 char)
 *                  - {boolean} parseHeader (defaults to true, i.e. 1st row is returned as an array at index 0.)
 *                  - {boolean} trim (defaults to true)
 * @return: {2D Array of strings}
 */
 
// Sample usage
var csv = '1,2,3\n4,5,6\n"Hello","World","!"';
var arrays = eve.csv2multiarrays(csv); // [[1,2,3], [4,5,6], ["Hello","World","!"]]
```

**`csv2array`**
```
/**
 * Parse a delimited string into an array of strings.
 * - Note: an empty string input will return an empty array
 * - Note: does not fully conform to RFC-4180, which allows multiple records
 *         this function only parses a CSV and returns a one-dimensional array
 *         i.e. only works on one record
 * @param: {string} data,
 *         {object} options (optional)
 *                  - {string} delimiter (defaults to comma. Must be 1 char)
 *                  - {string} quote (defaults to ". Must be 1 char)
 *                  - {boolean} trim (defaults to true)
 * @return: {Array of strings}
 */
 
// Sample usage
var csv = 'one,two,three';
var array = eve.csv2array(csv); //['one,two,three']
```

**`renderTemplate`**
```
/**
 * Renders a template by replacing all {{ }} and {{{ }}} occurrences with actual data
 * - Note: Any template string enclosed in double curly braces {{  }} will be automatically escaped.
 * - Note: Any template string enclosed in triple curly braces {{{  }}} will NOT be escaped.
 * @param: {string} template
 *         {object} key-value pairs
 */
// Sample usage
var template = '<div class="name">{{name}}</div><div class="email">{{email}}</div>';
var data = {name: 'John Doe', email: 'johndoe@gmail.com'};
var html = eve.renderTemplate(template, data); // <div class="name">John Doe</div><div class="email">johndoe@gmail.com</div>
```

**`encodeFileName`**
```
/**
 * onverts any non-alphanumeral / non-underscore / non-dot to dash.
 * - Note: output is a lower-case ASCII string
 * @param (string)
 * @return (string)
 */
 
// Sample usage
var file_name_by_user = 'My very own awesome image file!.JPG';
var file_name_by_system = eve.encodeFileName(file_name_by_user); // 'my-very-own-awesome-image-file-.jpg'
```

**`encodeFilePath`**
```
/**
 * Same as encodeFileName(), but preserves slashes, e.g. "/var/www/hello/world"
 * - Note: only works with forward slash /, does not work with backslash \
 * @param {string}
 * @return {string}
 */

// Sample usage
var file_path_by_user = 'C:/My Directory/image file!.JPG';
var file_path_by_system = eve.encodeFilePath(file_path_by_user); // 'c/my-directory/image-file-.jpg'
```

**`count`**
```
/**
 * Counts the occurrences of a string within another string
 * - Note: if needle is '' (empty), returns haystack length + 1
 * @param: {string} haystack
 *         {string} needle
 *         {object} userOptions, {allowOverlap}
 * @return: {number} an integer of count
 */
 
// Sample usage
var e_count = eve.count('Hello World!', 'e'); // 1
var o_count = eve.count('Hello World!', 'o'); // 2
```

**`levenshtein`**
```
/**
 * Calculates Levenshtein distance between two strings.
 * @param: {string}, {string}
 * @return: {number} integer
 */
 
// Sample usage
var dist = eve.levenshtein('kitten', 'kittah'); // 2
```

**`getInitials`**
```
/**
 * Gets the first letter of every word
 * - Note: if a string is shorter than min length, an error will be thrown.
 * - Note: if a string has fewer words than min length, letters from the first words will be used.
 * - Note: if a string has more words than max length, the first and last words will be used.
 * @param: {string}
 *         {object} options (optional)
 *             - {string} delimeter - default: ' ' (SPACE)
 *             - {int} minLength - defualt: 0 (no min)
 *             - {int} maxLength - defualt: str.length (no max)
 * @return: {string}
 */

// Sample usage
var initials = eve.getInitials('John Doe'); // JD
```

**`isInt`**
```
/***
 * Checks if a string/number is an int
 * - Note: strings / numbers like "12.0" evaluates to true
 * @param {string | number}
 * @return {bool}
 ***/
 
// Sample usage
if (!eve.isInt(input_value)) {
	alert('Input must be an integer');
}
```

**`isUInt`**
```
/***
 * Checks if a string/number (most probably from user input) is an unsigned integer.
 * - Note: strings / numbers like "12.0" evaluates to true
 * @param {string | number}
 * @return {bool}
 ***/

// Sample usage
if (!eve.isUInt(input_value)) {
	alert('Input must be a non-negative integer');
}
```

**`isNumber`**
```
/***
 * Checks for numbers (floats / integers)
 * @param {string | number}
 * @return {bool}
 ***/

// Sample usage
if (!eve.isNumber(input_value)) {
	alert('Input must be a number value');
}
```

**`isASCII`**
```
/**
 * Checks if string contains only ASCII printable characters
 * @param: {string}
 * @return: {bool}
 */
 
// Sample usage
if (!eve.isASCII(input_value)) {
	alert('We do not understand some words. Please type in English.');
}
```

**`isEmail`**
```
/***
 * Checks if a string is a valid email on the www
 * - e.g. TRUE for "test@example.co", FALSE for "test@localhost"
 * @param {string}
 * @return {bool}
 ***/

// Sample usage
if (!eve.isEmail(input_value)) {
	alert('Invalid email. Is there a typo somewhere?');
}
```

**`isURL`**
```
/***
 * Checks if a string is a valid URL on the www, e.g. TRUE for "http://www.example.com", FALSE for "http://localhost"
 * - Copyright (c) 2010-2013 Diego Perini (https://gist.github.com/dperini/729294)
 * @param {string}
 * @return {bool}
 ***/

// Sample usage
if (!eve.isURL(input_value)) {
	alert('Invalid URL. Is there a typo somewhere?');
}
```

**`shuffle`**
```
/**
 * Creates a shuffled copy of an Array / Set / string
 * @param: {Array | Set | string}
 * @return: {Array | Set | string}
 */

// Sample usage
var deck = ['A','K','Q','J','10'];
var shuffled_deck = eve.shuffle(deck);  // example: ['K', 'J', 'A', '10', 'Q']
```

**`union`**
```
/**
 * Computes union of multiple Sets (or array-like collections)
 * - primarily meant for Set
 * - all other objects, e.g. HTMLCollection, will return as Array
 * - Note: non-array-like objects are treated as empty arrays
 * @param: {Set | object}* - object should be an iterable collection
 * @return: {Set | Array}
 */

// Sample usage
var set1 = new Set();
set1.add(1);
set1.add(2);
var set2 = new Set();
set2.add(2);
set2.add(5);
var combined = eve.union(set1, set2); // Set now contains [1, 2, 5]
```

**`intersection`**
```
/**
 * Computes intersection of multiple Sets (or array-like collections)
 * - primarily meant for Set
 * - all other objects, e.g. HTMLCollection, will return as Array
 * - Note: non-array-like objects are treated as empty arrays
 * - Note: elements are compared by reference, so {'a':1} !== {'a':1}
 * @param: {Set | object}* - object should be an iterable collection
 * @return: {Set | Array}
 */

// Sample usage
var set1 = new Set();
set1.add(1);
set1.add(2);
var set2 = new Set();
set2.add(1);
set2.add(5);
var combined = eve.intersection(set1, set2); // Set now contains [1]
```

**`extend`**
```
/**
 * Extends a Map with other Maps, or an object with other objects.
 * Multiple objects may be passed in (non-objects are ignored).
 * - Note: the object in the first argument will be extended and returned.
 * - Note: values in later objects overwrite earlier objects.
 * - Note: when extending objects, all keys must be strings, else toString() will be automatically applied by JS engine
 * @param: {object, object, (object, ...)}
 * @return: {object}
 */
 
// Sample usage
var map1 = new Map();
map1.set(1, 'one');
map1.set(2, 'two');
var map2 = new Map();
map2.set(true, 'yes');
map2.set(false, 'no');
map2.set(1, 'single');
eve.extend(map1, map2); // map1 now has size: 4.
map1.get(1);              // 'single'
```

**`obj2map`**
```
/**
 * Returns a Map from an object
 * @param: {object}
 * @return: {Map}
 */
 
// Sample usage
var person = {
	'name': 'John Doe',
	'age' : 32,
	'sex' : 'Male'
};
var map1 = eve.obj2map(person); // map1 now contains 3 elems.
```

**`obj2set`**
```
/**
 * Returns a set from an object
 * @param: {object}
 * @return: {Set}
 */
 
// Sample usage
var person = {
	'name': 'John Doe',
	'age' : 32,
	'sex' : 'Male'
};
var set1 = eve.obj2set(person); // set1 now contains 3 elems: ['name','John Doe'], ['age',32], ['sex','Male'].
```

**`obj2array`**
```
/**
 * Returns an array from an object
 * @param: {object}
 * @return: {Array}
 */
 
// Sample usage
var person = {
	'name': 'John Doe',
	'age' : 32,
	'sex' : 'Male'
};
var arr1 = eve.obj2array(person); // arr1 now contains 3 elems: ['name','John Doe'], ['age',32], ['sex','Male'].
```

**`obj2csv`**
```
/**
 * Converts input object to a CSV string
 * - Note: toString() will be called on all properties / elements,
 *         null properties/elements will replaced by a string "null"
 * - Note: object will always result in a single line or double-line CSV, depending on parseHeader option
 * - Note: 2D arrays with records of consistent, positive length will be result in a multi-line CSV.
 * @param: {various} - can be object literal, Array, Set
 * @return: {string}
 */

// Sample usage
var records_array = [ ['name','age','sex'], ['John',30,'Male'], ['Jane',21,'Female'] ];
var csv = eve.obj2csv(records_array);
/* csv string contains:
name,age,sex
John,30,Male
Jane,21,Female
*/
var obj = {name: 'John', age: 32, sex:'Male'};
var csv = eve.obj2csv(obj);
/* csv string contains:
name,age,sex
John,32,Male
*/
```


### FAQ ###

1. Why don't you simply use existing libraries?
> We couldn't find any library with all these functions. When building applications, we frequently need to perform tasks like csv2array(), utf82ascii(). We wished there were reliable library functions for these common functions. Now there are.

1. Why doesn't this library include syntactic sugar functions like `isBlank()` or `isNull()`?
> While syntactic sugar is generlly fine, too much can be unhealthy. For example, a simple comparator test cam test if a string is blank; we do not need a library for that. We think it is a bad practice to use a function when a short line of code would suffice. Simply put, trivial "syntactic sugar" functions like `isEmpty()`, `isBlank()` don't appeal much to us.

1. Why doesn't this library provide polyfills for useful string functions like `includes()`, `startsWith()`, `endsWith()`?
> These functions are already available natively (ES-5 or ES-6 standards). Even for older browsers, these can and should be added using polyfills, something which a utility library should not be concerned with.

1. What's the difference between `slugify()` and `encodeFileName()`?
> `slugify()` removes non-word characters; `str2filename()` converts them to dashes.
>
> `slugify()` removes dots; `str2filename()` preserves them.
>
> `slugify()` will not produce strings beginning or ending with dashes; `str2filename()` may.
>
> `slugify()` will not produce strings containing consecutive dashes; `str2filename()` may.

1. Why don't you include function like `capitalize()` that changes each word's first letter to uppercase?
> Firstly, `capitalize()` is a poor name. Some libraries implement `capitalize()` as a function to "change ONLY the first letter to uppercase"" (like PHP's `ucfirst()`); in CSS, `capitalize` makes the first letter of every word uppercase. The different behaviour can be confusing.
>
> Secondly, and motr importantly, such a function is almost always used for pure presentational purposes. You can achieve the same effect simply by printing the string in an element with style `text-transform: capitalize` in CSS. We don't want to create functions that encourages developers to break the vaunted separation-of-concerns principle principle (for content and presentation).

1. Should I use `isInt()` or native ES6 `Number.isInteger()`? What's the difference?
> `Number.isInteger()` only works with type number, so passing in a string like '15' will return `false`.
> `isInt()` works with both numbers and strings, so passing in '15' will return `true`. This is useful for form validation as all form inputs are treated as strings.

1. Should I use `extend()` or native ES6 `Object.assign()`? What's the difference?
> `extend()` was never meant to replace `Object.assign()`. For most objects, definitely use `Object.assign()` to merge the properties.
> `extend()` is primarily meant for Maps. It is created because `Object.assign()` does not actually extend Maps. For flexibility, `extend()` also accepts objects, so that you can do things like extend a Map with object literals.

1. Does `union()` and `intersection()` work for Maps?
> Technically yes - you can pass in Maps since they are iterable, BUT note that they will be parsed into Arrays. These methods are primarily meant for Sets, where keys are not really important.

### Contact ###

* Email us at <yokestudio@hotmail.com>.