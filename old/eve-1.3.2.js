﻿/***
 * Library - Eve
 *
 * Useful methods for JS Strings, Collections, Object Arrays
 * 
 ***/
(function(global) {
	'use strict';
	
	/**********************************************
	 * String Utils (generic texts / HTML / URLs) *
	 *  - mostly expects one string param.        *
	 *    param will be casted to string if nec.  *
	 **********************************************/
	 
	/**
	 * Equivalent of PHP's htmlspecialchars(), e.g.
	 * '&' (ampersand) becomes '&amp;' 
	 * '<' (less than) becomes '&lt;'
	 * @param {string}
	 * @return {string}
	 */
	function escapeHTML(str) {
		if (typeof str !== 'string') {
			throw new TypeError('escapeHTML() - param is not str: ' + str);
		}
		return str
			.replace(/&/g, '&amp;')
			.replace(/</g, '&lt;')
			.replace(/>/g, '&gt;')
			.replace(/"/g, '&quot;')
			.replace(/'/g, '&#x27;')
			.replace(/\//g, '&#x2F;');
	} //escapeHTML()
	
	/**
	 * Inverse function of escapeHTML()
	 * @param {string}
	 * @return {string}
	 */
	function unescapeHTML(str) {
		if (typeof str !== 'string') {
			throw new TypeError('unescapeHTML() - param is not str: ' + str);
		}
		return str
			.replace(/&amp;/g,'&')
			.replace(/&lt;/g,'<')
			.replace(/&gt;/g,'>')
			.replace(/&quot;/g,'"')
			.replace(/&#x27;/g, '\'').replace(/&#039;/g, '\'')
			.replace(/&#x2F;/g, '\/').replace(/&#047;/g, '\/')
			.replace(/\u00a0/g, ' ');
	} //unescapeHTML()
	
	/**
	 * Encodes URL characters, e.g. spaces become %20, quotes become %22 etc.
	 * Adheres to RFC-3986.
	 * @param {string}
	 * @return {string}
	 */
	function encodeURL(str) {
		if (typeof str !== 'string') {
			throw new TypeError('encodeURL() - param is not str: ' + str);
		}
		return encodeURIComponent(str).replace(/[!'()*]/g, function(c) {
			return '%' + c.charCodeAt(0).toString(16).toUpperCase();
		});
	} //encodeURL()
	
	/**
	 * Decodes URL characters, e.g. %20 become spaces, %22 become quotes etc.
	 * @param {string}
	 * @return {string}
	 */
	function decodeURL(str) {
		if (typeof str !== 'string') {
			throw new TypeError('decodeURL() - param is not str: ' + str);
		}
		return decodeURIComponent(str
			.replace(/%21/g,'!')
			.replace(/%2A/g,'*')
			.replace(/%28/g,'(')
			.replace(/%29/g,')')
			.replace(/%27/g,'\'')
		);
	} //decodeURL()
	
	/**
	 * Changes all <br> and <br /> tags to newline "\n"
	 * @param {string}
	 * @return {string}
	 */
	function br2nl(str) {
		if (typeof str !== 'string') {
			throw new TypeError('br2nl() - param is not str: ' + str);
		}
		return str.replace(/<br\s*\/?>/mg,'\n');
	}
	
	/**
	 * Changes all newlines ["\r", "\n", \r\n"] to <br> tags
	 * @return (string)
	 */
	function nl2br(str) {
		if (typeof str !== 'string') {
			throw new TypeError('nl2br() - param is not str: ' + str);
		}
		return str.replace(/(?:\r\n|\r|\n)/g, '<br>');
	}
	
	/**
	 * Converts UTF-8 strings to ASCII by translating non-ASCII characters to the closest counterpart.
	 * @param {string}
	 * @return (string)
	 */
	function utf82ascii(str) {
		if (typeof str !== 'string') {
			throw new TypeError('utf82ascii() - param is not str: ' + str);
		}
		
		var defaultDiacriticsRemovalMap = [
			{'base':'A', 'letters':/[\u0041\u24B6\uFF21\u00C0\u00C1\u00C2\u1EA6\u1EA4\u1EAA\u1EA8\u00C3\u0100\u0102\u1EB0\u1EAE\u1EB4\u1EB2\u0226\u01E0\u00C4\u01DE\u1EA2\u00C5\u01FA\u01CD\u0200\u0202\u1EA0\u1EAC\u1EB6\u1E00\u0104\u023A\u2C6F]/g},
			{'base':'AA','letters':/[\uA732]/g},
			{'base':'AE','letters':/[\u00C6\u01FC\u01E2]/g},
			{'base':'AO','letters':/[\uA734]/g},
			{'base':'AU','letters':/[\uA736]/g},
			{'base':'AV','letters':/[\uA738\uA73A]/g},
			{'base':'AY','letters':/[\uA73C]/g},
			{'base':'B', 'letters':/[\u0042\u24B7\uFF22\u1E02\u1E04\u1E06\u0243\u0182\u0181]/g},
			{'base':'C', 'letters':/[\u0043\u24B8\uFF23\u0106\u0108\u010A\u010C\u00C7\u1E08\u0187\u023B\uA73E]/g},
			{'base':'D', 'letters':/[\u0044\u24B9\uFF24\u1E0A\u010E\u1E0C\u1E10\u1E12\u1E0E\u0110\u018B\u018A\u0189\uA779]/g},
			{'base':'DZ','letters':/[\u01F1\u01C4]/g},
			{'base':'Dz','letters':/[\u01F2\u01C5]/g},
			{'base':'E', 'letters':/[\u0045\u24BA\uFF25\u00C8\u00C9\u00CA\u1EC0\u1EBE\u1EC4\u1EC2\u1EBC\u0112\u1E14\u1E16\u0114\u0116\u00CB\u1EBA\u011A\u0204\u0206\u1EB8\u1EC6\u0228\u1E1C\u0118\u1E18\u1E1A\u0190\u018E]/g},
			{'base':'F', 'letters':/[\u0046\u24BB\uFF26\u1E1E\u0191\uA77B]/g},
			{'base':'G', 'letters':/[\u0047\u24BC\uFF27\u01F4\u011C\u1E20\u011E\u0120\u01E6\u0122\u01E4\u0193\uA7A0\uA77D\uA77E]/g},
			{'base':'H', 'letters':/[\u0048\u24BD\uFF28\u0124\u1E22\u1E26\u021E\u1E24\u1E28\u1E2A\u0126\u2C67\u2C75\uA78D]/g},
			{'base':'I', 'letters':/[\u0049\u24BE\uFF29\u00CC\u00CD\u00CE\u0128\u012A\u012C\u0130\u00CF\u1E2E\u1EC8\u01CF\u0208\u020A\u1ECA\u012E\u1E2C\u0197]/g},
			{'base':'J', 'letters':/[\u004A\u24BF\uFF2A\u0134\u0248]/g},
			{'base':'K', 'letters':/[\u004B\u24C0\uFF2B\u1E30\u01E8\u1E32\u0136\u1E34\u0198\u2C69\uA740\uA742\uA744\uA7A2]/g},
			{'base':'L', 'letters':/[\u004C\u24C1\uFF2C\u013F\u0139\u013D\u1E36\u1E38\u013B\u1E3C\u1E3A\u0141\u023D\u2C62\u2C60\uA748\uA746\uA780]/g},
			{'base':'LJ','letters':/[\u01C7]/g},
			{'base':'Lj','letters':/[\u01C8]/g},
			{'base':'M', 'letters':/[\u004D\u24C2\uFF2D\u1E3E\u1E40\u1E42\u2C6E\u019C]/g},
			{'base':'N', 'letters':/[\u004E\u24C3\uFF2E\u01F8\u0143\u00D1\u1E44\u0147\u1E46\u0145\u1E4A\u1E48\u0220\u019D\uA790\uA7A4]/g},
			{'base':'NJ','letters':/[\u01CA]/g},
			{'base':'Nj','letters':/[\u01CB]/g},
			{'base':'O', 'letters':/[\u004F\u24C4\uFF2F\u00D2\u00D3\u00D4\u1ED2\u1ED0\u1ED6\u1ED4\u00D5\u1E4C\u022C\u1E4E\u014C\u1E50\u1E52\u014E\u022E\u0230\u00D6\u022A\u1ECE\u0150\u01D1\u020C\u020E\u01A0\u1EDC\u1EDA\u1EE0\u1EDE\u1EE2\u1ECC\u1ED8\u01EA\u01EC\u00D8\u01FE\u0186\u019F\uA74A\uA74C]/g},
			{'base':'OI','letters':/[\u01A2]/g},
			{'base':'OO','letters':/[\uA74E]/g},
			{'base':'OU','letters':/[\u0222]/g},
			{'base':'P', 'letters':/[\u0050\u24C5\uFF30\u1E54\u1E56\u01A4\u2C63\uA750\uA752\uA754]/g},
			{'base':'Q', 'letters':/[\u0051\u24C6\uFF31\uA756\uA758\u024A]/g},
			{'base':'R', 'letters':/[\u0052\u24C7\uFF32\u0154\u1E58\u0158\u0210\u0212\u1E5A\u1E5C\u0156\u1E5E\u024C\u2C64\uA75A\uA7A6\uA782]/g},
			{'base':'S', 'letters':/[\u0053\u24C8\uFF33\u1E9E\u015A\u1E64\u015C\u1E60\u0160\u1E66\u1E62\u1E68\u0218\u015E\u2C7E\uA7A8\uA784]/g},
			{'base':'T', 'letters':/[\u0054\u24C9\uFF34\u1E6A\u0164\u1E6C\u021A\u0162\u1E70\u1E6E\u0166\u01AC\u01AE\u023E\uA786]/g},
			{'base':'TZ','letters':/[\uA728]/g},
			{'base':'U', 'letters':/[\u0055\u24CA\uFF35\u00D9\u00DA\u00DB\u0168\u1E78\u016A\u1E7A\u016C\u00DC\u01DB\u01D7\u01D5\u01D9\u1EE6\u016E\u0170\u01D3\u0214\u0216\u01AF\u1EEA\u1EE8\u1EEE\u1EEC\u1EF0\u1EE4\u1E72\u0172\u1E76\u1E74\u0244]/g},
			{'base':'V', 'letters':/[\u0056\u24CB\uFF36\u1E7C\u1E7E\u01B2\uA75E\u0245]/g},
			{'base':'VY','letters':/[\uA760]/g},
			{'base':'W', 'letters':/[\u0057\u24CC\uFF37\u1E80\u1E82\u0174\u1E86\u1E84\u1E88\u2C72]/g},
			{'base':'X', 'letters':/[\u0058\u24CD\uFF38\u1E8A\u1E8C]/g},
			{'base':'Y', 'letters':/[\u0059\u24CE\uFF39\u1EF2\u00DD\u0176\u1EF8\u0232\u1E8E\u0178\u1EF6\u1EF4\u01B3\u024E\u1EFE]/g},
			{'base':'Z', 'letters':/[\u005A\u24CF\uFF3A\u0179\u1E90\u017B\u017D\u1E92\u1E94\u01B5\u0224\u2C7F\u2C6B\uA762]/g},
			{'base':'a', 'letters':/[\u0061\u24D0\uFF41\u1E9A\u00E0\u00E1\u00E2\u1EA7\u1EA5\u1EAB\u1EA9\u00E3\u0101\u0103\u1EB1\u1EAF\u1EB5\u1EB3\u0227\u01E1\u00E4\u01DF\u1EA3\u00E5\u01FB\u01CE\u0201\u0203\u1EA1\u1EAD\u1EB7\u1E01\u0105\u2C65\u0250]/g},
			{'base':'aa','letters':/[\uA733]/g},
			{'base':'ae','letters':/[\u00E6\u01FD\u01E3]/g},
			{'base':'ao','letters':/[\uA735]/g},
			{'base':'au','letters':/[\uA737]/g},
			{'base':'av','letters':/[\uA739\uA73B]/g},
			{'base':'ay','letters':/[\uA73D]/g},
			{'base':'b', 'letters':/[\u0062\u24D1\uFF42\u1E03\u1E05\u1E07\u0180\u0183\u0253]/g},
			{'base':'c', 'letters':/[\u0063\u24D2\uFF43\u0107\u0109\u010B\u010D\u00E7\u1E09\u0188\u023C\uA73F\u2184]/g},
			{'base':'d', 'letters':/[\u0064\u24D3\uFF44\u1E0B\u010F\u1E0D\u1E11\u1E13\u1E0F\u0111\u018C\u0256\u0257\uA77A]/g},
			{'base':'dz','letters':/[\u01F3\u01C6]/g},
			{'base':'e', 'letters':/[\u0065\u24D4\uFF45\u00E8\u00E9\u00EA\u1EC1\u1EBF\u1EC5\u1EC3\u1EBD\u0113\u1E15\u1E17\u0115\u0117\u00EB\u1EBB\u011B\u0205\u0207\u1EB9\u1EC7\u0229\u1E1D\u0119\u1E19\u1E1B\u0247\u025B\u01DD]/g},
			{'base':'f', 'letters':/[\u0066\u24D5\uFF46\u1E1F\u0192\uA77C]/g},
			{'base':'g', 'letters':/[\u0067\u24D6\uFF47\u01F5\u011D\u1E21\u011F\u0121\u01E7\u0123\u01E5\u0260\uA7A1\u1D79\uA77F]/g},
			{'base':'h', 'letters':/[\u0068\u24D7\uFF48\u0125\u1E23\u1E27\u021F\u1E25\u1E29\u1E2B\u1E96\u0127\u2C68\u2C76\u0265]/g},
			{'base':'hv','letters':/[\u0195]/g},
			{'base':'i', 'letters':/[\u0069\u24D8\uFF49\u00EC\u00ED\u00EE\u0129\u012B\u012D\u00EF\u1E2F\u1EC9\u01D0\u0209\u020B\u1ECB\u012F\u1E2D\u0268\u0131]/g},
			{'base':'j', 'letters':/[\u006A\u24D9\uFF4A\u0135\u01F0\u0249]/g},
			{'base':'k', 'letters':/[\u006B\u24DA\uFF4B\u1E31\u01E9\u1E33\u0137\u1E35\u0199\u2C6A\uA741\uA743\uA745\uA7A3]/g},
			{'base':'l', 'letters':/[\u006C\u24DB\uFF4C\u0140\u013A\u013E\u1E37\u1E39\u013C\u1E3D\u1E3B\u017F\u0142\u019A\u026B\u2C61\uA749\uA781\uA747]/g},
			{'base':'lj','letters':/[\u01C9]/g},
			{'base':'m', 'letters':/[\u006D\u24DC\uFF4D\u1E3F\u1E41\u1E43\u0271\u026F]/g},
			{'base':'n', 'letters':/[\u006E\u24DD\uFF4E\u01F9\u0144\u00F1\u1E45\u0148\u1E47\u0146\u1E4B\u1E49\u019E\u0272\u0149\uA791\uA7A5]/g},
			{'base':'nj','letters':/[\u01CC]/g},
			{'base':'o', 'letters':/[\u006F\u24DE\uFF4F\u00F2\u00F3\u00F4\u1ED3\u1ED1\u1ED7\u1ED5\u00F5\u1E4D\u022D\u1E4F\u014D\u1E51\u1E53\u014F\u022F\u0231\u00F6\u022B\u1ECF\u0151\u01D2\u020D\u020F\u01A1\u1EDD\u1EDB\u1EE1\u1EDF\u1EE3\u1ECD\u1ED9\u01EB\u01ED\u00F8\u01FF\u0254\uA74B\uA74D\u0275]/g},
			{'base':'oi','letters':/[\u01A3]/g},
			{'base':'ou','letters':/[\u0223]/g},
			{'base':'oo','letters':/[\uA74F]/g},
			{'base':'p','letters':/[\u0070\u24DF\uFF50\u1E55\u1E57\u01A5\u1D7D\uA751\uA753\uA755]/g},
			{'base':'q','letters':/[\u0071\u24E0\uFF51\u024B\uA757\uA759]/g},
			{'base':'r','letters':/[\u0072\u24E1\uFF52\u0155\u1E59\u0159\u0211\u0213\u1E5B\u1E5D\u0157\u1E5F\u024D\u027D\uA75B\uA7A7\uA783]/g},
			{'base':'s','letters':/[\u0073\u24E2\uFF53\u00DF\u015B\u1E65\u015D\u1E61\u0161\u1E67\u1E63\u1E69\u0219\u015F\u023F\uA7A9\uA785\u1E9B]/g},
			{'base':'t','letters':/[\u0074\u24E3\uFF54\u1E6B\u1E97\u0165\u1E6D\u021B\u0163\u1E71\u1E6F\u0167\u01AD\u0288\u2C66\uA787]/g},
			{'base':'tz','letters':/[\uA729]/g},
			{'base':'u','letters':/[\u0075\u24E4\uFF55\u00F9\u00FA\u00FB\u0169\u1E79\u016B\u1E7B\u016D\u00FC\u01DC\u01D8\u01D6\u01DA\u1EE7\u016F\u0171\u01D4\u0215\u0217\u01B0\u1EEB\u1EE9\u1EEF\u1EED\u1EF1\u1EE5\u1E73\u0173\u1E77\u1E75\u0289]/g},
			{'base':'v','letters':/[\u0076\u24E5\uFF56\u1E7D\u1E7F\u028B\uA75F\u028C]/g},
			{'base':'vy','letters':/[\uA761]/g},
			{'base':'w','letters':/[\u0077\u24E6\uFF57\u1E81\u1E83\u0175\u1E87\u1E85\u1E98\u1E89\u2C73]/g},
			{'base':'x','letters':/[\u0078\u24E7\uFF58\u1E8B\u1E8D]/g},
			{'base':'y','letters':/[\u0079\u24E8\uFF59\u1EF3\u00FD\u0177\u1EF9\u0233\u1E8F\u00FF\u1EF7\u1E99\u1EF5\u01B4\u024F\u1EFF]/g},
			{'base':'z','letters':/[\u007A\u24E9\uFF5A\u017A\u1E91\u017C\u017E\u1E93\u1E95\u01B6\u0225\u0240\u2C6C\uA763]/g}
		];
	
		for (var i=0, len=defaultDiacriticsRemovalMap.length; i<len; i++) {
			str = str.replace(defaultDiacriticsRemovalMap[i].letters, defaultDiacriticsRemovalMap[i].base);
		}
	
		return str;
	} //utf82ascii()
	
	/**
	 * Converts a string to a slug, e.g. "Hello World" ==> "hello-world"
	 * - Note: output is a lower-case ASCII string, and does not begin/end with dash
	 * @param: {string}
	 * @return: {string}
	 */
	function slugify(str) {
		if (typeof str !== 'string') {
			throw new TypeError('slugify() - param is not str: ' + str);
		}
		
		return utf82ascii(str)
			.toLowerCase()
			.replace(/\s+/g, '-')     // Replace spaces with dash
			.replace(/[^\w\-]+/g, '') // Remove all non-word chars
			.replace(/\-\-+/g, '-')   // Collapse dashes
			.trim()                     // Trim spaces
			.replace(/^[-]+|[-]+$/g,''); // Trim dashes
	} //slugify()
	
	/**
	 * Parse a CSV string into a 2D array of strings.
	 * conform to RFC-4180, which allows multiple records (each a CSV)
	 * @param: {string} data,
	 *         {object} options (optional)
	 *                  - {string} delimiter (defaults to comma. Must be 1 char)
	 *                  - {string} lineEnding (defaults to \n. Max of 2 chars)
	 *                  - {string} quote {defaults to ". Must be 1 char)
	 *                  - {boolean} parseHeader (defaults to true, i.e. 1st row is returned as an array at index 0.)
	 *                  - {boolean} trim (defaults to true)
	 * @return: {2D Array of strings}
	 */
	function csv2multiarrays(data, options) {
		if (typeof data !== 'string') {
			throw new TypeError('csv2multiarray() - data is not str: ' + data);
		}
		if (typeof options !== 'object' || options === null) {
			options = {
				delimeter: ',',
				lineEnding: '\n',
				quote: '"',
				trim: true,
				parseHeader: true
			};
		}
		if (typeof options.delimeter !== 'string' || options.delimeter.length !== 1) {
			options.delimeter = ',';
		}
		if (typeof options.lineEnding !== 'string' || options.lineEnding.length > 2 || options.lineEnding.length < 1) {
			options.lineEnding = '\n';
		}
		if (typeof options.quote !== 'string' || options.quote.length !== 1) {
			options.quote = '"';
		}
		if (typeof options.trim !== 'boolean') {
			options.trim = true;
		}
		if (typeof options.parseHeader !== 'boolean') {
			options.parseHeader = true;
		}
		var lineEndingChar1 = options.lineEnding.charAt(0);
		var lineEndingChar2 = options.lineEnding.charAt(1);
		
		var result = [['']];
		var row = 0;
		var col = 0;
		var inside_quotes = false;
		var expect_delimeter_or_eof = false;
		
		// Go through each character
		var i,len; // tmp variables for loops
		for (i=0,len=data.length;i<len;i++) {
			var c = data.charAt(i);
			switch (c) {
				case options.quote:
					if (inside_quotes && data.charAt(i+1) === options.quote) {
						result[row][col] += options.quote;
						++i;
					} // escaped quote
					else if (!inside_quotes && result[row][col].length > 0) {
						throw new TypeError('csv2multiarray() - data is not valid csv. Quotes cannot appear here: Line ' + (row+1) + ', char ' + (i+1)); 
					} // check that no characters appear before/after quotes
					else {
						inside_quotes = !inside_quotes;
					}
					
					if (!inside_quotes) {
						expect_delimeter_or_eof = true;
					}
					break;
					
				case options.delimeter:
					if (inside_quotes) {
						result[row][col] += c;
					}
					else {
						if (options.trim) {
							result[row][col] = String(result[row][col]).trim();
						}
						col++;
						result[row][col] = '';
						
						expect_delimeter_or_eof = false;
					}
					break;
					
				case lineEndingChar1:
					if (!inside_quotes && (!lineEndingChar2 || (lineEndingChar2 && lineEndingChar2 === data.charAt(i+1)))) {
						if (options.trim) {
							result[row][col] = String(result[row][col]).trim();
						}
						row++;
						col = 0;
						result[row] = [''];
						if (lineEndingChar2) {
							++i;
						}
						
						expect_delimeter_or_eof = false;
					} // we have a line-ending we need to process
					else {
						result[row][col] += c;
					}
					break;
				
				default:
					if (expect_delimeter_or_eof) {
						throw new TypeError('csv2multiarray() - data is not valid csv. Expecting delimeter at char ' + i + ' (row: ' + row + ', col: ' + col);
					}
					result[row][col] += c;
					break;
			}
		} // for each character
		
		// check that quotes are closed
		if (inside_quotes) {
			throw new TypeError('csv2multiarray() - data is not valid csv. Unclosed quote detected.');
		}
		
		// Last item may not have been trimmed
		if (options.trim) {
			result[row][col] = String(result[row][col]).trim();
		}
		
		
		if (!options.parseHeader) {
			result.shift();
		} // no headers necessary
		else {
			// Check that each row contains the same number of fields
			for (i=1; i<result.length; i++) { // note: cannot cache length
				if (result[i].length !== result[0].length) {
					result.splice(i,1);
					i--; // compensate index due to splice
				}
			}
		} // header is present

		return result;
	} //csv2multiarrays()
	
	/**
	 * Parse a delimited string into an array of strings.
	 * - Note: an empty string input will return an empty array
	 * - Note: does not fully conform to RFC-4180, which allows multiple records
	 *         this function only parses a CSV and returns a one-dimensional array
	 *         i.e. only works on one record
	 * @param: {string} data,
	 *         {object} options (optional)
	 *                  - {string} delimiter (defaults to comma. Must be 1 char)
	 *                  - {string} quote (defaults to ". Must be 1 char)
	 *                  - {boolean} trim (defaults to true)
	 * @return: {Array of strings}
	 */
	function csv2array(data, options){
		if (typeof data !== 'string') {
			throw new TypeError('csv2array() - data is not str');
		}
		if (typeof options !== 'object' || options === null) {
			options = {
				delimeter: ',',
				quote: '"',
				trim: true
			};
		}
		if (typeof options.delimeter !== 'string' || options.delimeter.length !== 1) {
			options.delimeter = ',';
		}
		if (typeof options.quote !== 'string' || options.quote.length !== 1) {
			options.quote = '"';
		}
		if (typeof options.trim !== 'boolean') {
			options.trim = true;
		}
				
		var result = []; // result to return
		var value = ''; // current value parsed so far
		var inside_quotes = false;
		var expect_delimeter_or_eof = false; // after exiting quotes, we only expect delimeter or EOF
		
		// Go through each character
		var i,len; // tmp variables for loops
		for (i=0,len=data.length;i<len;i++) {
			var c = data.charAt(i);
			
			// Process char
			if (c === options.delimeter) {
				// check if we're inside quotes
				if (inside_quotes) {
					value += ',';
					continue;
				}

				// push this value into result array
				if (options.trim) {
					result.push(value.trim());
				}
				else {
					result.push(value);
				}
				
				// reset to original state
				value = '';
				expect_delimeter_or_eof = false;
			} // ecountered delimeter
			else if (c === options.quote) {
				if (inside_quotes && data.charAt(i+1) === options.quote) {
					value += options.quote;
					++i;
				} // escaped quote
				else if (!inside_quotes && value.length > 0) {
					throw new TypeError('csv2array() - data is not valid csv. Quotes cannot appear here: char ' + (i+1)); 
				} // check that no characters appear before/after quotes
				else {
					inside_quotes = !inside_quotes;
				}
					
				if (!inside_quotes) {
					expect_delimeter_or_eof = true;
				}
			} // encountered a double-quote
			else {
				if (expect_delimeter_or_eof) {
					if (!options.trim || c !== ' ') {
						throw new TypeError('csv2array() - data is not a csv (Unexpected data after double-quotes)');
					}
				}
				value += c;
			}
		} // for each char
		
		// check that quotes are closed
		if (inside_quotes) {
			throw new TypeError('csv2array() - data is not valid csv. Unclosed quote detected.');
		}
		
		// push last value into result array
		if (options.trim) {
			value = value.trim();
		}
		result.push(value);
		
		return result;
	} //csv2array()

	/**
	 * Renders a template by replacing all {{ }} and {{{ }}} occurrences with actual data
	 * - Note: Any template string enclosed in double curly braces {{  }} will be automatically escaped.
	 * - Note: Any template string enclosed in triple curly braces {{{  }}} will NOT be escaped.
	 * @param: {string} template
	 *         {object} key-value pairs
	 * @return: {string}
	 */
	function renderTemplate(template, data) {
		if (typeof template !== 'string') {
			throw new TypeError('renderTemplate() - template is not a string.');
		}
		if (typeof data !== 'object') {
			throw new TypeError('renderTemplate() - data is not an object.');
		}

		// Replace all templates found in data
		for (var key in data) {
			if (!data.hasOwnProperty(key)) {
				continue;
			}

			// Don't render objects
			if (typeof data[key] === 'object') {
				continue;
			}

			var escaped = escapeHTML('' + data[key]);
			var double_regex = new RegExp('{{' + key + '}}', 'g');
			var triple_regex = new RegExp('{{{' + key + '}}}', 'g');
			template = template.replace(triple_regex, data[key]).replace(double_regex, escaped);
		}

		// Remove all unused templates
		return template.replace(/\{\{\{(?:(?!}}).)*\}\}\}/g, '').replace(/\{\{(?:(?!}}).)*\}\}/g, '');
	} //renderTemplate()

	/**
	 * Converts any non-alphanumeral / non-underscore / non-dot to dash.
	 * - Note: output is a lower-case ASCII string
	 * @param (string)
	 * @return (string)
	 */
	function encodeFileName(n) {
		if (typeof n !== 'string') {
			throw new TypeError('encodeFileName() - param is not str');
		}
		return utf82ascii(n)
			.replace(/[^a-z0-9_\.-]/gi, '-')
			.toLowerCase();
	} //encodeFileName()
	
	/**
	 * Pads a string with chars until it hits certain length
	 * @param: {string} str - original string
	 *         {number} len - desired length of string (integer)
	 *         {string} c - the character to pad
	 * @return {string}
	 */
	function padLeft(str, len, c) {
		if (typeof str !== 'string') {
			throw new TypeError('padLeft() - 1st param is not str');
		}
		if (typeof len !== 'number' || len%1 !== 0) {
			throw new TypeError('padLeft() - 2nd param is not an int');
		}
		if (typeof c !== 'string') {
			throw new TypeError('padLeft() - 3rd param is not str');
		}
		
		while (str.length < len) {
			str = c + str;
		}
		return str;
	}
	function padRight(str, len, c) {
		if (typeof str !== 'string') {
			throw new TypeError('padRight() - 1st param is not str');
		}
		if (typeof len !== 'number' || len%1 !== 0) {
			throw new TypeError('padRight() - 2nd param is not an int');
		}
		if (typeof c !== 'string') {
			throw new TypeError('padRight() - 3rd param is not str');
		}
		
		while (str.length < len) {
			str = str + c;
		}
		return str;
	}
	
	/**
	 * Counts the occurrences of a string within another string
	 * - Note: if needle is '' (empty), returns haystack length + 1
	 * @param: {string} haystack
	 *         {string} needle
	 *         {object} userOptions, {allowOverlap}
	 * @return: {number} an integer of count
	 */
	function count(haystack, needle, options) {
		// Data checks
		if (typeof haystack !== 'string') {
			throw new TypeError('count() - 1st param is not str');
		}
		if (typeof needle !== 'string') {
			throw new TypeError('count() - 2nd param is not str');
		}
		
		// Trivial case
		if (needle.length <= 0) {
			return haystack.length + 1;
		}
		
		// Check Options
		if (typeof options !== 'object' || options === null) {
			options = {
				allowOverlap: false
			};
		}
		if (typeof options.allowOverlap !== 'boolean') {
			options.allowOverlap = false;
		}
		
		var count = 0;
		var pos = 0;
		var step = (options.allowOverlap) ? 1 : (needle.length);
		
		while (true) {
			pos = haystack.indexOf(needle, pos);
			
			if (pos >= 0) {
			  count++;
			  pos += step;
			}
			else {
				break;
			}
		}
		
		return count;
	} //count()
	
	/**
	 * Calculates Levenshtein distance between two strings.
	 * @param: {string}, {string}
	 * @return: {number} integer
	 */
	function levenshtein(str1, str2) {
		if (typeof str1 !== 'string') {
			throw new TypeError('levenshtein() - str1 is not string');
		}
		if (typeof str2 !== 'string') {
			throw new TypeError('levenshtein() - str2 is not string');
		}
		
		// Trivial cases
		if (str1 === str2) {
			return 0;
		}
		var str1_len = str1.length;
		var str2_len = str2.length;
		if (str1_len === 0) {
			return str2_len;
		}
		if (str2_len === 0) {
			return str1_len;
		}
		
		var i,j, tmp; // loop vars
		
		// initialise previous row
		var prev_row = [];
		for (i = 0; i <= str2_len; i++) {
			prev_row[i] = i;
		}
		
		// Calculate current row distance from previous row
		var next_col;
		for (i = 0; i < str1_len; i++) {
			next_col = i + 1;

			// go through each char of str2
			for (j = 0; j < str2_len; j++) {
				var cur_col = next_col;

				// substitution
				next_col = prev_row[j] + ( (str1.charAt(i) === str2.charAt(j)) ? 0 : 1 );
				
				// insertion
				tmp = cur_col + 1;
				if (next_col > tmp) {
					next_col = tmp;
				}
				
				// deletion
				tmp = prev_row[j + 1] + 1;
				if (next_col > tmp) {
					next_col = tmp;
				}

				// copy cur_col value into previous (in preparation for next iteration)
				prev_row[j] = cur_col;
			}

			// copy last col value into previous (in preparation for next iteration)
			prev_row[j] = next_col;
		}

		return next_col;
	} //levenshtein()
	
	/**
	 * Gets the first letter of every word
	 * - Note: if a string is shorter than min length, an error will be thrown.
	 * - Note: if a string has fewer words than min length, letters from the first words will be used.
	 * - Note: if a string has more words than max length, the first and last words will be used.
	 * @param: {string}
	 *         {object} options (optional)
	 *             - {string} delimeter - default: ' ' (SPACE)
	 *             - {int} minLength - defualt: 0 (no min)
	 *             - {int} maxLength - defualt: str.length (no max)
	 * @return: {string}
	 */
	function getInitials(str, options) {
		if (typeof str !== 'string') {
			throw new TypeError('getInitials() - param is not a string: ' + str);
		}

		// Check Options
		if (typeof options !== 'object' || options === null) {
			options = {
				delimeter: ' '
			};
		}
		if (typeof options['delimeter'] !== 'string') {
			options['delimeter'] = ' ';
		}
		var words = str.split(options['delimeter']);
		var letter_count = str.length - words.length + 1;

		if (typeof options['maxLength'] !== 'number' || options['maxLength'] > letter_count) {
			options['maxLength'] = letter_count;
		}
		if (typeof options['minLength'] !== 'number' || options['minLength'] < 0) {
			options['minLength'] = 0;
		}
		if (options['minLength'] > letter_count) {
			throw new RangeError('getInitials() - not enough letters to produce initials of minLength ' + options['minLength'] + ' for: ' + str);
		}
		if (options['minLength'] > options['maxLength']) {
			throw new RangeError('getInitials() - options minLength ' + options['minLength'] + ' cannot be more than maxLength ' + options['maxLength']);
		}

		//console.log('DEBUG: ===' + str + '===');

		// Trivial case
		if (str === '') {
			return '';
		}

		// Prepare indices
		var front_word_index = 0; // index to run loop from front to back
		var back_word_index = words.length - 1; // index to run loop from back to front
		var take_from_front = false; // toggling flag
		var front_initials = words[0].charAt(0);
		var back_initials = '';

		// Check if we need more than just the first letters
		var additional_count = options['minLength'] - words.length;
		var curr_word_length = words[0].length;
		var curr_char_index = 1;
		while (additional_count > 0) {
			if (curr_char_index === curr_word_length) {
				front_word_index++;
				additional_count++; // remember to increment for each word we go past
				curr_word_length = words[front_word_index].length;
				curr_char_index = 0;
			} // word has no more letters

			front_initials = front_initials + words[front_word_index].charAt(curr_char_index);
			additional_count--;
			curr_char_index++;
		}
		//console.log('DEBUG: front initials: ' + front_initials );
		if (front_initials !== '') {
			front_word_index++; // remember to move on since we have already "consumed" this word
		}

		// Find initials until we hit maxLength
		while (front_word_index <= back_word_index && front_initials.length + back_initials.length < options['maxLength']) {
			if (take_from_front) {
				front_initials = front_initials + words[front_word_index].charAt(0);
				front_word_index++;
			}
			else {
				back_initials = words[back_word_index].charAt(0) + back_initials;
				back_word_index--;
			}
			take_from_front = !take_from_front;
		} // while

		return front_initials + back_initials;
	} //getInitials()

	/**
	 * Checks if a string/number is an int
	 * - Note: strings / numbers like "12.0" evaluates to true
	 * @param {string | number}
	 * @return {bool}
	 */
	function isInt(str) {
		if (typeof str !== 'number' && typeof str !== 'string') {
			return false;
		}
		
		return str % 1 === 0;
	}
	
	/**
	 * Checks if a string/number (most probably from user input) is an unsigned integer.
	 * - Note: strings / numbers like "12.0" evaluates to true
	 * @param {string | number}
	 * @return {bool}
	 */
	function isUInt(str) {
		if (typeof str !== 'number' && typeof str !== 'string') {
			return false;
		}
		
		/*
		var intRegex = /^\d+$/;
		return intRegex.test(str);
		*/
		return str % 1 === 0 && str >= 0;
	}
	
	/**
	 * Checks for numbers (floats / integers)
	 * @param {string | number}
	 * @return {bool}
	 */
	function isNumber(str) {
		if (typeof str !== 'number' && typeof str !== 'string') {
			return false;
		}
		
		return !isNaN(parseFloat(str)) && isFinite(str);
		
		// jQuery
		//return (str - parseFloat(str) + 1) >= 0;
	}
	
	/**
	 * Checks if string contains only ASCII printable characters
	 * @param: {string}
	 * @return: {bool}
	 */
	function isASCII(str) {
		if (typeof str !== 'string') {
			return false;
		}
		
		if (str === '') {
			return true;
		}
		
		var ascii = /^[\x00-\x7F]+$/;
		return ascii.test(str);
	} //isASCII()
	
	/**
	 * Checks if a string is a valid email on the www
	 * - e.g. TRUE for "test@example.co", FALSE for "test@localhost"
	 * @param {string}
	 * @return {bool}
	 */
	function isEmail(str) {
		if (typeof str !== 'string') {
			return false;
		}
		
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(str);
	} //isEmail()
	
	/**
	 * Checks if a string is a valid URL on the www, e.g. TRUE for "http://www.example.com", FALSE for "http://localhost"
	 * - Copyright (c) 2010-2013 Diego Perini (https://gist.github.com/dperini/729294)
	 * @param {string}
	 * @return {bool}
	 */
	function isURL(str) {
		if (typeof str !== 'string') {
			return false;
		}
		
		var pattern = new RegExp('^' +
			'(?:(?:https?|ftp)://)' +  // protocol identifier
			'(?:\\S+(?::\\S*)?@)?' +   // user:pass authentication
			'(?:' +
				// IP address exclusion
				// private & local networks
				'(?!(?:10|127)(?:\\.\\d{1,3}){3})' +
				'(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})' +
				'(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})' +
				// IP address dotted notation octets
				// excludes loopback network 0.0.0.0
				// excludes reserved space >= 224.0.0.0
				// excludes network & broacast addresses
				// (first & last IP address of each class)
				'(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])' +
				'(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}' +
				'(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))' +
			'|' +
				// host name
				'(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)' +
				// domain name
				'(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*' +
				// TLD identifier
				'(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))' +
				// TLD may end with dot
				'.?' +
			')' +
			// port number
			'(?::\\d{2,5})?' +
			// resource path
			'(?:[/?#]\\S*)?' +
			'$', 'i'
		);

		return pattern.test(str);
	} //isURL()
	
	/**********************************************
	 * Collections / Objects                      *
	 *  - expects Set, Array, HTMLCollecton etc.  *
	 *  - also works with object literals         *
	 **********************************************/
	/**
	 * Creates a shuffled copy of an Array / Set / string
	 * @param: {Array | Set | string}
	 * @return: {Array | Set | string}
	 */
	function shuffle(obj) {
		var type = typeof obj;
		if (type !== 'object' && type !== 'string') {
			throw new TypeError('shuffle() - cannot process: ' + type);
		}
		if (obj === null) {
			throw new TypeError('shuffle() - cannot process null.');
		}
		
		// Create an array of the original object (we'll be removing elems from it)
		var operated;
		if (type === 'string') {
			operated = obj.split('');
		}
		else if (obj instanceof Array) {
			operated = obj.slice();
		}
		else if (obj instanceof Set) {
			operated = set2array(obj);
		}
		else {
			throw new TypeError('shuffle() - can only process Array / Set.');
		}
		
		var result = [];
		while (operated.length > 0) {
			var i = Math.floor(Math.random() * operated.length);
			result.push(operated[i]);
			operated.splice(i, 1); // note: modifies array in place
		}
		
		if (type === 'string') {
			return result.join('');
		}
		else if (obj instanceof Array) {
			return result;
		}
		else {
			var set = new Set();
			result.forEach(function(v){
				set.add(v);
			});
			return set;
		}
	} //shuffle()
	
	/**
	 * Computes union of multiple Sets (or array-like collections)
	 * - Primarily meant for Sets. All other objects, e.g. HTMLCollection, will return as Array
	 * - Note: all duplicate values are removed
	 * - Note: non-array-like objects are treated as empty arrays
	 * @param: {Set | object}* - object should be an iterable collection
	 * @return: {Set | Array}
	 */
	function union() {
		var i, arg;
		var len = arguments.length;
		var result;
		
		// Check args
		if (len === 0 || len === 1) {
			throw new TypeError('union() expects at least 2 params');
		}
		if (typeof arguments[0] !== 'object' || arguments[0] === null) {
			throw new TypeError('union() expects 1st param to be a non-null object');
		}
		if (typeof arguments[1] !== 'object' || arguments[1] === null) {
			throw new TypeError('union() expects 2nd param to be a non-null object');
		}
		
		result = arrayLikeObj2set(arguments[0]);
		
		// Add each argument's items into result Set
		for (i=1; i<len; i++) {
			arg = arguments[i];
			if (typeof arg === 'undefined' || arg === null) {
				throw new TypeError('union() expects non-null arguments. arg ' + i + ' is null/undefined.');
			}
			
			if (!(arg instanceof Set)) {
				arg = arrayLike2array(arg);
			}
			arg.forEach(addToResult);
		}
		function addToResult(v) {
			result.add(v);
		}
		
		// Return either Set or Array
		return (arguments[0] instanceof Set) ? result : set2array(result);
	} //union()
	
	/**
	 * Computes intersection of multiple Sets (or array-like collections)
	 * - primarily meant for Set
	 * - all other objects, e.g. HTMLCollection, will return as Array
	 * - Note: non-array-like objects are treated as empty arrays
	 * - Note: elements are compared by reference, so {'a':1} !== {'a':1}
	 * @param: {Set | object}* - object should be an iterable collection
	 * @return: {Set | Array}
	 */
	function intersection() {
		var i,tmp0,tmp1;
		var len = arguments.length;
		var result;
		
		// Check args
		if (len === 0 || len === 1) {
			throw new TypeError('intersection() expects at least 2 params');
		}
		if (typeof arguments[0] !== 'object' || arguments[0] === null) {
			throw new TypeError('intersection() expects 1st param to be a non-null object');
		}
		if (typeof arguments[1] !== 'object' || arguments[1] === null) {
			throw new TypeError('intersection() expects 2nd param to be a non-null object');
		}
		
		// Edge case: only 2 args
		if (len === 2) {
			// Convert args to sets
			tmp0 = arrayLikeObj2set(arguments[0]);
			tmp1 = arrayLikeObj2set(arguments[1]);
			//console.log(set2array(tmp0));
			//console.log(set2array(tmp1));
			
			result = new Set();
			
			// Add common elements to result
			tmp0.forEach(function(v) {
				if (tmp1.has(v)) {
					result.add(v);
				}
			});
			
			// Return either Set or Array
			return (arguments[0] instanceof Set) ? result : set2array(result);
		}
		
		// More than 2 args
		result = arrayLikeObj2set(arguments[0]);
		
		for (i=1; i<len; i++) {
			if (typeof arguments[i] === 'undefined' || arguments[i] === null) {
				throw new TypeError('intersection() expects non-null iterable arguments. arg ' + i + ' is null/undefined.');
			}
			result = intersection(result, arguments[i]);
		}
		
		// Return either Set or Array
		return (arguments[0] instanceof Set) ? result : set2array(result);
	} //intersection()
	
	/**
	 * Extends a Map with other Maps, or an object with other objects.
	 * Multiple objects may be passed in (non-objects are ignored).
	 * - Note: the object in the first argument will be extended and returned.
	 * - Note: values in later objects overwrite earlier objects.
	 * - Note: when extending objects, all keys must be strings, else toString() will be automatically applied by JS engine
	 * @param: {object, object, (object, ...)}
	 * @return: {object}
	 */
	function extend() {
		var i;
		var len = arguments.length;
		
		// Check args
		if (len === 0 || len === 1) {
			throw new TypeError('extend() expects at least 2 params');
		}
		if (typeof arguments[0] !== 'object' || arguments[0] === null) {
			throw new TypeError('extend() expects 1st param to be a non-null object');
		}
		
		// Extend arguments[0] with the other objects
		var obj = arguments[0];
		for (i = 1; i < len; i++) {
			var arg = arguments[i];
			
			// ignore non-objects
			if (typeof arg !== 'object') {
				continue;
			}
			
			if (arg instanceof Map || arg instanceof Set || arg instanceof Array) {
				if (obj instanceof Map) {
					arg.forEach(extendMap);
				} // extending Map with Map/Array
				else {
					arg.forEach(extendObj);
				} // extending generic object with Map/Array
			} // arg is Map / Array
			else {
				for (var key in arg) {
					if (!arg.hasOwnProperty(key)) {
						continue;
					}
					
					if (obj instanceof Map) {
						obj.set(key, arg[key]);
					} // extending Map with obj prop
					else {
						obj[key] = arg[key];
					} // extending generic object with object
				}
			} // arg is generic object
		}

		function extendMap(v, k) {
			obj.set(k,v);
		}
		function extendObj(v, k) {
			obj[k] = v;
		}

		return obj;
	} //extend()
	
	/**
	 * Returns a Map from an object
	 * @param: {object}
	 * @return: {Map}
	 */
	function obj2map(obj) {
		if (typeof obj !== 'object' || obj === null) {
			throw new TypeError('obj2map() - expects non-null objects');
		}
		return extend(new Map(), obj);
	}
	
	/**
	 * Returns a set from an object
	 * @param: {object}
	 * @return: {Set}
	 */
	function obj2set(obj) {
		if (typeof obj !== 'object' || obj === null) {
			throw new TypeError('obj2set() - expects non-null array-like objects');
		}
		
		return arrayLikeObj2set(obj2array(obj));
	}
	
	/**
	 * Returns an array from an object
	 * TODO: handle arrayLike objects
	 * @param: {object}
	 * @return: {Array}
	 */
	function obj2array(obj) {
		if (typeof obj !== 'object' || obj === null) {
			throw new TypeError('obj2array() - expects non-null objects');
		}
		if (obj instanceof Array) {
			return obj.slice();
		}
		else if (obj instanceof Set) {
			return set2array(obj);
		}
		else if (obj instanceof Map) {
			return map2array(obj);
		}
		else {
			return map2array(obj2map(obj));
		}
	}
	
	/**
	 * Converts input object to a CSV string
	 * - Note: toString() will be called on all properties / elements,
	 *         null properties/elements will replaced by a string "null"
	 * - Note: object will always result in a single line or double-line CSV, depending on parseHeader option
	 * - Note: 2D arrays with records of consistent, positive length will be result in a multi-line CSV.
	 * @param: {object} - can be object literal, Array, Set
	 * @return: {string}
	 */
	function obj2csv(obj, options) {
		// Process options
		if (typeof options !== 'object' || options === null) {
			options = {
				delimeter: ',',
				lineEnding: '\n',
				quote: '"',
				trim: true,
				parseHeader: true
			};
		}
		if (typeof options.delimeter !== 'string' || options.delimeter.length !== 1) {
			options.delimeter = ',';
		}
		if (typeof options.lineEnding !== 'string' || options.lineEnding.length > 2 || options.lineEnding.length < 1) {
			options.lineEnding = '\n';
		}
		if (typeof options.quote !== 'string' || options.quote.length !== 1) {
			options.quote = '"';
		}
		if (typeof options.trim !== 'boolean') {
			options.trim = true;
		}
		if (typeof options.parseHeader !== 'boolean') {
			options.parseHeader = true;
		}
		
		// Return base on type
		if (typeof obj !== 'object' || obj === null) {
			throw new TypeError('obj2csv() - expects non-null objects');
		}
		else if (obj instanceof Array) {
			var i;
			var len = obj.length;
			if (len === 0) {
				return '';
			}
			
			// Check if we are dealing with a 2D array
			var is_multi_array = true;
			for (i=0; i<len; i++) {
				if (!(obj[i] instanceof Array) || obj[i].length !== obj[0].length || obj[i].length === 0) {
					is_multi_array = false;
					break;
				} // each item must be an array, and have the exact same length.
			}
			
			if (is_multi_array) {
				return multiarray2csv(obj, options);
			}
			else {
				return array2csv(obj, options);
			}
		}
		else if (obj instanceof Set) {
			return obj2csv(set2array(obj), options);
		}
		else if (obj instanceof Map) {
			// Note: cannot simply convert map to array, which produces elems of length === 2. We want keys to be an array
			var keys = [];
			var values = [];
			obj.forEach(function(v, k) {
				keys.push(k);
				values.push(v);
			});
			return obj2csv([keys, values], options);
		}
		else {
			return objLiteral2csv(obj, options);
		}
	} //obj2csv()
	
	// @private
	function multiarray2csv(arr, options) {
		var i;
		var len = arr.length;
		
		var result = '';
		if (options.parseHeader) {
			result += array2csv(arr[0], options) + options.lineEnding;
		}
		
		for (i=1; i<len; i++) {
			result += array2csv(arr[i], options) + options.lineEnding;
		}
		
		return result.slice(0, -options.lineEnding.length);
	} //multiarray2csv()
	
	// @private
	function array2csv(arr, options) {
		var i, curr;
		var len = arr.length;
		if (len === 0) {
			return '';
		}
		
		var result = '';
		for (i=0; i<len; i++) {
			// get a string value of current item
			curr = arr[i];
			if (curr === null) {
				curr = 'null';
				//throw new TypeError('obj2csv() - encountered null while processing array2csv on element ' + i);
			}
			else if (typeof curr === 'object') {
				curr = JSON.stringify(curr);
			}
			else {
				curr = curr.toString();
			}
			
			// add item to result
			if (curr.indexOf(options.delimeter) !== -1 || curr.indexOf(options.quote) !== -1 || curr.indexOf(options.lineEnding) !== -1) {
				var regex = new RegExp(options.quote, 'g');
				result +=
					options.quote +
					curr.replace(regex, options.quote+options.quote) + // escape quotes
					options.quote + options.delimeter;
			} // values containing line endings, quotes and commas must be enclosed
			else {
				result += curr + options.delimeter;
			}
		}
		
		return result.slice(0, -options.delimeter.length); // remove last delimeter
	} //array2csv()
	
	// @private
	function objLiteral2csv(obj, options) {
		var key, curr; // loop vars
		
		var result = '';
		
		// Parse Header
		if (options.parseHeader) {
			for (key in obj) {
				if (!obj.hasOwnProperty(key)) {
					continue;
				}
			
				// get string value of current item
				curr = key.toString(); // note: keys can be strings / numbers
				
				// add item to result
				if (curr.indexOf(options.delimeter) !== -1 || curr.indexOf(options.quote) !== -1 || curr.indexOf(options.lineEnding) !== -1) {
					result +=
						options.quote +
						curr.replace(new RegExp(options.quote, 'g'), options.quote+options.quote) + // escape quotes
						options.quote + options.delimeter;
				} // values containing line endings, quotes and commas must be enclosed
				else {
					result += curr + options.delimeter;
				}
			}
			
			result = result.slice(0, -options.delimeter.length) + options.lineEnding; // remove last delimeter, add line ending
		} // parse header
		
		// Parse properties
		for (key in obj) {
			if (!obj.hasOwnProperty(key)) {
				continue;
			}
			
			// get string value of current item
			curr = obj[key];
			if (curr === null) {
				curr = 'null';
			}
			else if (typeof curr === 'object') {
				curr = JSON.stringify(curr);
			}
			else {
				curr = curr.toString();
			}
			
			// add item to result
			if (curr.indexOf(options.delimeter) !== -1 || curr.indexOf(options.quote) !== -1 || curr.indexOf(options.lineEnding) !== -1) {
				result +=
					options.quote +
					curr.replace(new RegExp(options.quote, 'g'), options.quote+options.quote) + // escape quotes
					options.quote + options.delimeter;
			} // values containing line endings, quotes and commas must be enclosed
			else {
				result += curr + options.delimeter;
			}
		} // for
		
		return result.slice(0, -options.delimeter.length); // remove last delimeter
	} //objLiteral2csv()
	 
	/**
	 * @private
	 * We may need this because Array.from() does not work for Sets reliably across all browsers.
	 * @param: {Set}
	 * @return: {Array}
	 */
	function set2array(s) {
		var a = [];
		s.forEach(function(v){
			a.push(v);
		});
		return a;
	} //set2array()
	
	/**
	 * @private
	 * We may need this because Array.from() does not work for Maps reliably across all browsers.
	 * @param: {Map}
	 * @return: {Array}
	 */
	function map2array(s) {
		var a = [];
		s.forEach(function(v,k){
			a.push([k,v]);
		});
		return a;
	} //map2array()
	
	/**
	 * @private
	 * Converts array-like objects or strings into arrays
	 */
	function arrayLike2array(a) {
		var result = [];
		if (a instanceof Set) {
			a.forEach(function(v){
				result.push(v);
			});
			return result;
		}
		else if (a instanceof Map) {
			a.forEach(function(v,k){
				result.push([k, v]);
			});
			return result;
		}
		else {
			return [].slice.call(a);
		}	
	}
	
	/**
	 * @private
	 * Converts array-like objects to Set
	 */
	function arrayLikeObj2set(a) {
		if (!(a instanceof Set)) {
			a = arrayLike2array(a);
		}
		
		var s = new Set(); // result
		a.forEach(function(v) {
			s.add(v);
		});
		
		return s;
	}
	
	global.eve = {
		/* String Utils */                 /* Aliases */
		escapeHTML: escapeHTML,            htmlspecialchars: escapeHTML,
		unescapeHTML: unescapeHTML,        htmlspecialchars_decode: unescapeHTML,
		encodeURL: encodeURL,              urlencode: encodeURL,
		decodeURL: decodeURL,              urldecode: decodeURL,
		br2nl: br2nl,
		nl2br: nl2br,
		utf82ascii: utf82ascii,
		slugify: slugify,
		csv2array: csv2array,
		csv2multiarrays: csv2multiarrays,
		encodeFileName: encodeFileName,
		renderTemplate: renderTemplate,
		padLeft: padLeft,
		padRight: padRight,
		count: count,
		levenshtein: levenshtein,
		getInitials: getInitials,
		isInt: isInt,
		isUInt: isUInt,                   isUnsignedInt: isUInt,
		isNumber: isNumber,               isNumeric: isNumber,
		isASCII: isASCII,
		isEmail: isEmail,
		isURL: isURL,
		
		/* Collection / Object Utils */   /* Aliases */
		shuffle: shuffle,
		union: union,
		intersection: intersection,
		extend: extend,
		obj2map: obj2map,
		obj2set: obj2set,
		obj2array: obj2array,
		obj2csv: obj2csv
	};
})(this);
