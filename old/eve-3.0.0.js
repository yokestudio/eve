/***
 * Library - Eve
 *
 * Useful methods for JS Strings, Collections, Object Arrays
 ***/
(function(global) {
	'use strict';

/*********************************************
* String Utils (generic texts / HTML / URLs) *
*  - mostly expects one string param.        *
*    param will be casted to string if nec.  *
**********************************************/
	/**
	 * Equivalent of PHP's htmlspecialchars(), e.g.
	 * '&' (ampersand) becomes '&amp;'
	 * '<' (less than) becomes '&lt;'
	 * @param {string} str
	 * @return {string}
	 */
	function escapeHTML(str) {
		if (typeof str !== 'string') {
			throw new TypeError('escapeHTML() - param is not str: ' + str);
		}

		return str
		.replace(/&/g, '&amp;')
		.replace(/</g, '&lt;')
		.replace(/>/g, '&gt;')
		.replace(/"/g, '&quot;')
		.replace(/'/g, '&#x27;')
		.replace(/\//g, '&#x2F;');
	} // escapeHTML()

	/**
	 * Inverse function of escapeHTML()
	 * @param {string} str
	 * @return {string}
	 */
	function unescapeHTML(str) {
		if (typeof str !== 'string') {
			throw new TypeError('unescapeHTML() - param is not str: ' + str);
		}

		return str
		.replace(/&amp;/g, '&')
		.replace(/&lt;/g, '<')
		.replace(/&gt;/g, '>')
		.replace(/&quot;/g, '"')
		.replace(/&#x27;/g, '\'')
		.replace(/&#039;/g, '\'')
		.replace(/&#x2F;/g, '/')
		.replace(/&#047;/g, '/')
		.replace(/\u00a0/g, ' ');
	} // unescapeHTML()

	/**
	 * Encodes URL characters, e.g. spaces become %20, quotes become %22 etc.
	 * Adheres to RFC-3986 (https://www.ietf.org/rfc/rfc3986.txt), which the native encodeURI() does not.
	 * @param {string} str
	 * @return {string}
	 */
	function encodeURL(str) {
		if (typeof str !== 'string') {
			throw new TypeError('encodeURL() - param is not str: ' + str);
		}

		return encodeURI(str).replace(/%5B/g, '[')
		.replace(/%5D/g, ']'); // undo escapes on square brackets, in accordance with RFC-3986.
	} // encodeURL()

	/**
	 * Encodes URL component characters, e.g. spaces become %20, quotes become %22 etc.
	 * Adheres to RFC-3986 (https://www.ietf.org/rfc/rfc3986.txt), which the native encodeURIComponent() does not.
	 * @param {string} str
	 * @return {string}
	 */
	function encodeURLComponent(str) {
		if (typeof str !== 'string') {
			throw new TypeError('encodeURLComponent() - param is not str: ' + str);
		}

		// Also encode additional 5 reserved characters, in accordance with RFC-3986
		return encodeURIComponent(str).replace(/[!'()*]/g, function(c) {
			return '%' + c.charCodeAt(0).toString(16) // eslint-disable-line no-magic-numbers
			.toUpperCase();
		});
	} // encodeURLComponent()

	/**
	 * Decodes URL characters, e.g. %20 become spaces, %22 become quotes etc.
	 * Adheres to RFC-3986 (https://www.ietf.org/rfc/rfc3986.txt), which the native decodeURI() does not.
	 * @param {string} str
	 * @return {string}
	 */
	function decodeURL(str) {
		if (typeof str !== 'string') {
			throw new TypeError('decodeURL() - param is not str: ' + str);
		}

		return decodeURI(str);
	} // decodeURL()

	/**
	 * Decodes URL component characters, e.g. %20 become spaces, %22 become quotes etc.
	 * Adheres to RFC-3986 (https://www.ietf.org/rfc/rfc3986.txt), which the native decodeURIComponent() does not.
	 * @param {string} str
	 * @return {string}
	 */
	function decodeURLComponent(str) {
		if (typeof str !== 'string') {
			throw new TypeError('decodeURLComponent() - param is not str: ' + str);
		}

		return decodeURIComponent(str
		.replace(/%21/g, '!')
		.replace(/%2A/g, '*')
		.replace(/%28/g, '(')
		.replace(/%29/g, ')')
		.replace(/%27/g, '\''));
	} // decodeURLComponent()

	/**
	 * Changes all <br> and <br /> tags to newline "\n"
	 * @param {string} str
	 * @return {string}
	 */
	function br2nl(str) {
		if (typeof str !== 'string') {
			throw new TypeError('br2nl() - param is not str: ' + str);
		}

		return str.replace(/<br\s*\/?>/mg, '\n');
	} // br2nl()

	/**
	 * Changes all newlines ["\r", "\n", \r\n"] to <br> tags
	 * @return (string) str
	 */
	function nl2br(str) {
		if (typeof str !== 'string') {
			throw new TypeError('nl2br() - param is not str: ' + str);
		}

		return str.replace(/(?:\r\n|\r|\n)/g, '<br>');
	} // nl2br()

	/**
	 * Converts UTF-8 strings to ASCII [0-127] by translating non-ASCII characters to the closest counterpart.
	 * - If no suitable replacement exists, a character outside of ASCII range is mapped to underscore.
	 * - reference: https://github.com/mplatt/fold-to-ascii
	 * @param {string} str
	 * @return (string)
	 */
	function utf82ascii(str) {
		if (typeof str !== 'string') {
			throw new TypeError('utf82ascii() - param is not str: ' + str);
		}

		/* eslint-disable no-magic-numbers, max-len */
		let map = new Map([
			[0xC0, 'A'], [0xC1, 'A'], [0xC2, 'A'], [0xC3, 'A'], [0xC4, 'A'], [0xC5, 'A'], [0x100, 'A'], [0x102, 'A'], [0x104, 'A'], [0x18F, 'A'], [0x1CD, 'A'], [0x1DE, 'A'],
			[0x1E0, 'A'], [0x1FA, 'A'], [0x200, 'A'], [0x202, 'A'], [0x226, 'A'], [0x23A, 'A'], [0x1D00, 'A'], [0x1E00, 'A'], [0x1EA0, 'A'], [0x1EA2, 'A'], [0x1EA4, 'A'],
			[0x1EA6, 'A'], [0x1EA8, 'A'], [0x1EAA, 'A'], [0x1EAC, 'A'], [0x1EAE, 'A'], [0x1EB0, 'A'], [0x1EB2, 'A'], [0x1EB4, 'A'], [0x1EB6, 'A'], [0x24B6, 'A'], [0xFF21, 'A'],
			[0xE0, 'a'], [0xE1, 'a'], [0xE2, 'a'], [0xE3, 'a'], [0xE4, 'a'], [0xE5, 'a'], [0x101, 'a'], [0x103, 'a'], [0x105, 'a'], [0x1CE, 'a'], [0x1DF, 'a'], [0x1E1, 'a'],
			[0x1FB, 'a'], [0x201, 'a'], [0x203, 'a'], [0x227, 'a'], [0x250, 'a'], [0x259, 'a'], [0x25A, 'a'], [0x1D8F, 'a'], [0x1D95, 'a'], [0x1E01, 'a'], [0x1E9A, 'a'],
			[0x1EA1, 'a'], [0x1EA3, 'a'], [0x1EA5, 'a'], [0x1EA7, 'a'], [0x1EA9, 'a'], [0x1EAB, 'a'], [0x1EAD, 'a'], [0x1EAF, 'a'], [0x1EB1, 'a'], [0x1EB3, 'a'], [0x1EB5, 'a'],
			[0x1EB7, 'a'], [0x2090, 'a'], [0x2094, 'a'], [0x24D0, 'a'], [0x2C65, 'a'], [0x2C6F, 'a'], [0xFF41, 'a'], [0xA732, 'AA'], [0xC6, 'AE'], [0x1E2, 'AE'], [0x1FC, 'AE'],
			[0x1D01, 'AE'], [0xA734, 'AO'], [0xA736, 'AU'], [0xA738, 'AV'], [0xA73A, 'AV'], [0xA73C, 'AY'], [0x249C, '(a)'], [0xA733, 'aa'], [0xE6, 'ae'], [0x1E3, 'ae'],
			[0x1FD, 'ae'], [0x1D02, 'ae'], [0xA735, 'ao'], [0xA737, 'au'], [0xA739, 'av'], [0xA73B, 'av'], [0xA73D, 'ay'],
			[0x181, 'B'], [0x182, 'B'], [0x243, 'B'], [0x299, 'B'], [0x1D03, 'B'], [0x1E02, 'B'], [0x1E04, 'B'], [0x1E06, 'B'], [0x24B7, 'B'], [0xFF22, 'B'],
			[0x180, 'b'], [0x183, 'b'], [0x253, 'b'], [0x1D6C, 'b'], [0x1D80, 'b'], [0x1E03, 'b'], [0x1E05, 'b'], [0x1E07, 'b'], [0x24D1, 'b'], [0xFF42, 'b'], [0x249D, '(b)'],
			[0xC7, 'C'], [0x106, 'C'], [0x108, 'C'], [0x10A, 'C'], [0x10C, 'C'], [0x187, 'C'], [0x23B, 'C'], [0x297, 'C'], [0x1D04, 'C'], [0x1E08, 'C'], [0x24B8, 'C'], [0xFF23, 'C'],
			[0xE7, 'c'], [0x107, 'c'], [0x109, 'c'], [0x10B, 'c'], [0x10D, 'c'], [0x188, 'c'], [0x23C, 'c'], [0x255, 'c'], [0x1E09, 'c'], [0x2184, 'c'], [0x24D2, 'c'], [0xA73E, 'c'],
			[0xA73F, 'c'], [0xFF43, 'c'], [0x249E, '(c)'],
			[0xD0, 'D'], [0x10E, 'D'], [0x110, 'D'], [0x189, 'D'], [0x18A, 'D'], [0x18B, 'D'], [0x1D05, 'D'], [0x1D06, 'D'], [0x1E0A, 'D'], [0x1E0C, 'D'], [0x1E0E, 'D'],
			[0x1E10, 'D'], [0x1E12, 'D'], [0x24B9, 'D'], [0xA779, 'D'], [0xFF24, 'D'],
			[0xF0, 'd'], [0x10F, 'd'], [0x111, 'd'], [0x18C, 'd'], [0x221, 'd'], [0x256, 'd'], [0x257, 'd'], [0x1D6D, 'd'], [0x1D81, 'd'], [0x1D91, 'd'], [0x1E0B, 'd'],
			[0x1E0D, 'd'], [0x1E0F, 'd'], [0x1E11, 'd'], [0x1E13, 'd'], [0x24D3, 'd'], [0xA77A, 'd'], [0xFF44, 'd'],
			[0x1C4, 'DZ'], [0x1F1, 'DZ'], [0x1C5, 'Dz'], [0x1F2, 'Dz'], [0x249F, '(d)'], [0x238, 'db'], [0x1C6, 'dz'], [0x1F3, 'dz'], [0x2A3, 'dz'], [0x2A5, 'dz'],
			[0xC8, 'E'], [0xC9, 'E'], [0xCA, 'E'], [0xCB, 'E'], [0x112, 'E'], [0x114, 'E'], [0x116, 'E'], [0x118, 'E'], [0x11A, 'E'], [0x18E, 'E'], [0x190, 'E'], [0x204, 'E'],
			[0x206, 'E'], [0x228, 'E'], [0x246, 'E'], [0x1D07, 'E'], [0x1E14, 'E'], [0x1E16, 'E'], [0x1E18, 'E'], [0x1E1A, 'E'], [0x1E1C, 'E'], [0x1EB8, 'E'], [0x1EBA, 'E'],
			[0x1EBC, 'E'], [0x1EBE, 'E'], [0x1EC0, 'E'], [0x1EC2, 'E'], [0x1EC4, 'E'], [0x1EC6, 'E'], [0x24BA, 'E'], [0x2C7B, 'E'], [0xFF25, 'E'],
			[0xE8, 'e'], [0xE9, 'e'], [0xEA, 'e'], [0xEB, 'e'], [0x113, 'e'], [0x115, 'e'], [0x117, 'e'], [0x119, 'e'], [0x11B, 'e'], [0x1DD, 'e'], [0x205, 'e'], [0x207, 'e'],
			[0x229, 'e'], [0x247, 'e'], [0x258, 'e'], [0x25B, 'e'], [0x25C, 'e'], [0x25D, 'e'], [0x25E, 'e'], [0x29A, 'e'], [0x1D08, 'e'], [0x1D92, 'e'], [0x1D93, 'e'],
			[0x1D94, 'e'], [0x1E15, 'e'], [0x1E17, 'e'], [0x1E19, 'e'], [0x1E1B, 'e'], [0x1E1D, 'e'], [0x1EB9, 'e'], [0x1EBB, 'e'], [0x1EBD, 'e'], [0x1EBF, 'e'], [0x1EC1, 'e'],
			[0x1EC3, 'e'], [0x1EC5, 'e'], [0x1EC7, 'e'], [0x2091, 'e'], [0x24D4, 'e'], [0x2C78, 'e'], [0xFF45, 'e'], [0x24A0, '(e)'],
			[0x191, 'F'], [0x1E1E, 'F'], [0x24BB, 'F'], [0xA730, 'F'], [0xA77B, 'F'], [0xA7FB, 'F'], [0xFF26, 'F'],
			[0x192, 'f'], [0x1D6E, 'f'], [0x1D82, 'f'], [0x1E1F, 'f'], [0x1E9B, 'f'], [0x24D5, 'f'], [0xA77C, 'f'], [0xFF46, 'f'], [0x24A1, '(f)'], [0xFB00, 'ff'], [0xFB03, 'ffi'],
			[0xFB04, 'ffl'], [0xFB01, 'fi'], [0xFB02, 'fl'],
			[0x11C, 'G'], [0x11E, 'G'], [0x120, 'G'], [0x122, 'G'], [0x193, 'G'], [0x1E4, 'G'], [0x1E5, 'G'], [0x1E6, 'G'], [0x1E7, 'G'], [0x1F4, 'G'], [0x262, 'G'], [0x29B, 'G'],
			[0x1E20, 'G'], [0x24BC, 'G'], [0xA77D, 'G'], [0xA77E, 'G'], [0xFF27, 'G'],
			[0x11D, 'g'], [0x11F, 'g'], [0x121, 'g'], [0x123, 'g'], [0x1F5, 'g'], [0x260, 'g'], [0x261, 'g'], [0x1D77, 'g'], [0x1D79, 'g'], [0x1D83, 'g'], [0x1E21, 'g'],
			[0x24D6, 'g'], [0xA77F, 'g'], [0xFF47, 'g'], [0x24A2, '(g)'],
			[0x124, 'H'], [0x126, 'H'], [0x21E, 'H'], [0x29C, 'H'], [0x1E22, 'H'], [0x1E24, 'H'], [0x1E26, 'H'], [0x1E28, 'H'], [0x1E2A, 'H'], [0x24BD, 'H'], [0x2C67, 'H'],
			[0x2C75, 'H'], [0xFF28, 'H'],
			[0x125, 'h'], [0x127, 'h'], [0x21F, 'h'], [0x265, 'h'], [0x266, 'h'], [0x2AE, 'h'], [0x2AF, 'h'], [0x1E23, 'h'], [0x1E25, 'h'], [0x1E27, 'h'], [0x1E29, 'h'],
			[0x1E2B, 'h'], [0x1E96, 'h'], [0x24D7, 'h'], [0x2C68, 'h'], [0x2C76, 'h'], [0xFF48, 'h'], [0x1F6, 'HV'], [0x24A3, '(h)'], [0x195, 'hv'],
			[0xCC, 'I'], [0xCD, 'I'], [0xCE, 'I'], [0xCF, 'I'], [0x128, 'I'], [0x12A, 'I'], [0x12C, 'I'], [0x12E, 'I'], [0x130, 'I'], [0x196, 'I'], [0x197, 'I'], [0x1CF, 'I'],
			[0x208, 'I'], [0x20A, 'I'], [0x26A, 'I'], [0x1D7B, 'I'], [0x1E2C, 'I'], [0x1E2E, 'I'], [0x1EC8, 'I'], [0x1ECA, 'I'], [0x24BE, 'I'], [0xA7FE, 'I'], [0xFF29, 'I'],
			[0xEC, 'i'], [0xED, 'i'], [0xEE, 'i'], [0xEF, 'i'], [0x129, 'i'], [0x12B, 'i'], [0x12D, 'i'], [0x12F, 'i'], [0x131, 'i'], [0x1D0, 'i'], [0x209, 'i'], [0x20B, 'i'],
			[0x268, 'i'], [0x1D09, 'i'], [0x1D62, 'i'], [0x1D7C, 'i'], [0x1D96, 'i'], [0x1E2D, 'i'], [0x1E2F, 'i'], [0x1EC9, 'i'], [0x1ECB, 'i'], [0x2071, 'i'], [0x24D8, 'i'],
			[0xFF49, 'i'], [0x132, 'IJ'], [0x24A4, '(i)'], [0x133, 'ij'],
			[0x134, 'J'], [0x248, 'J'], [0x1D0A, 'J'], [0x24BF, 'J'], [0xFF2A, 'J'], [0x135, 'j'], [0x1F0, 'j'], [0x237, 'j'], [0x249, 'j'], [0x25F, 'j'], [0x284, 'j'], [0x29D, 'j'],
			[0x24D9, 'j'], [0x2C7C, 'j'], [0xFF4A, 'j'], [0x24A5, '(j)'],
			[0x136, 'K'], [0x198, 'K'], [0x1E8, 'K'], [0x1D0B, 'K'], [0x1E30, 'K'], [0x1E32, 'K'], [0x1E34, 'K'], [0x24C0, 'K'], [0x2C69, 'K'], [0xA740, 'K'], [0xA742, 'K'],
			[0xA744, 'K'], [0xFF2B, 'K'], [0x137, 'k'], [0x199, 'k'], [0x1E9, 'k'], [0x29E, 'k'], [0x1D84, 'k'], [0x1E31, 'k'], [0x1E33, 'k'], [0x1E35, 'k'], [0x24DA, 'k'],
			[0x2C6A, 'k'], [0xA741, 'k'], [0xA743, 'k'], [0xA745, 'k'], [0xFF4B, 'k'], [0x24A6, '(k)'],
			[0x139, 'L'], [0x13B, 'L'], [0x13D, 'L'], [0x13F, 'L'], [0x141, 'L'], [0x23D, 'L'], [0x29F, 'L'], [0x1D0C, 'L'], [0x1E36, 'L'], [0x1E38, 'L'], [0x1E3A, 'L'], [0x1E3C, 'L'],
			[0x24C1, 'L'], [0x2C60, 'L'], [0x2C62, 'L'], [0xA746, 'L'], [0xA748, 'L'], [0xA780, 'L'], [0xFF2C, 'L'],
			[0x13A, 'l'], [0x13C, 'l'], [0x13E, 'l'], [0x140, 'l'], [0x142, 'l'], [0x19A, 'l'], [0x234, 'l'], [0x26B, 'l'], [0x26C, 'l'], [0x26D, 'l'], [0x1D85, 'l'], [0x1E37, 'l'],
			[0x1E39, 'l'], [0x1E3B, 'l'], [0x1E3D, 'l'], [0x24DB, 'l'], [0x2C61, 'l'], [0xA747, 'l'], [0xA749, 'l'], [0xA781, 'l'], [0xFF4C, 'l'],
			[0x1C7, 'LJ'], [0x1EFA, 'LL'], [0x1C8, 'Lj'], [0x24A7, '(l)'], [0x1C9, 'lj'], [0x1EFB, 'll'], [0x2AA, 'ls'], [0x2AB, 'lz'],
			[0x19C, 'M'], [0x1D0D, 'M'], [0x1E3E, 'M'], [0x1E40, 'M'], [0x1E42, 'M'], [0x24C2, 'M'], [0x2C6E, 'M'], [0xA7FD, 'M'], [0xA7FF, 'M'], [0xFF2D, 'M'],
			[0x26F, 'm'], [0x270, 'm'], [0x271, 'm'], [0x1D6F, 'm'], [0x1D86, 'm'], [0x1E3F, 'm'], [0x1E41, 'm'], [0x1E43, 'm'], [0x24DC, 'm'], [0xFF4D, 'm'], [0x24A8, '(m)'],
			[0xD1, 'N'], [0x143, 'N'], [0x145, 'N'], [0x147, 'N'], [0x14A, 'N'], [0x19D, 'N'], [0x1F8, 'N'], [0x220, 'N'], [0x274, 'N'], [0x1D0E, 'N'], [0x1E44, 'N'], [0x1E46, 'N'],
			[0x1E48, 'N'], [0x1E4A, 'N'], [0x24C3, 'N'], [0xFF2E, 'N'], [0xF1, 'n'], [0x144, 'n'], [0x146, 'n'], [0x148, 'n'], [0x149, 'n'], [0x14B, 'n'], [0x19E, 'n'], [0x1F9, 'n'],
			[0x235, 'n'], [0x272, 'n'], [0x273, 'n'], [0x1D70, 'n'], [0x1D87, 'n'], [0x1E45, 'n'], [0x1E47, 'n'], [0x1E49, 'n'], [0x1E4B, 'n'], [0x207F, 'n'], [0x24DD, 'n'],
			[0xFF4E, 'n'], [0x1CA, 'NJ'], [0x1CB, 'Nj'], [0x24A9, '(n)'], [0x1CC, 'nj'],
			[0xD2, 'O'], [0xD3, 'O'], [0xD4, 'O'], [0xD5, 'O'], [0xD6, 'O'], [0xD8, 'O'], [0x14C, 'O'], [0x14E, 'O'], [0x150, 'O'], [0x186, 'O'], [0x19F, 'O'], [0x1A0, 'O'], [0x1D1, 'O'],
			[0x1EA, 'O'], [0x1EC, 'O'], [0x1FE, 'O'], [0x20C, 'O'], [0x20E, 'O'], [0x22A, 'O'], [0x22C, 'O'], [0x22E, 'O'], [0x230, 'O'], [0x1D0F, 'O'], [0x1D10, 'O'], [0x1E4C, 'O'],
			[0x1E4E, 'O'], [0x1E50, 'O'], [0x1E52, 'O'], [0x1ECC, 'O'], [0x1ECE, 'O'], [0x1ED0, 'O'], [0x1ED2, 'O'], [0x1ED4, 'O'], [0x1ED6, 'O'], [0x1ED8, 'O'], [0x1EDA, 'O'],
			[0x1EDC, 'O'], [0x1EDE, 'O'], [0x1EE0, 'O'], [0x1EE2, 'O'], [0x24C4, 'O'], [0xA74A, 'O'], [0xA74C, 'O'], [0xFF2F, 'O'], [0xF2, 'o'], [0xF3, 'o'], [0xF4, 'o'], [0xF5, 'o'],
			[0xF6, 'o'], [0xF8, 'o'], [0x14D, 'o'], [0x14F, 'o'], [0x151, 'o'], [0x1A1, 'o'], [0x1D2, 'o'], [0x1EB, 'o'], [0x1ED, 'o'], [0x1FF, 'o'], [0x20D, 'o'], [0x20F, 'o'],
			[0x22B, 'o'], [0x22D, 'o'], [0x22F, 'o'], [0x231, 'o'], [0x254, 'o'], [0x275, 'o'], [0x1D16, 'o'], [0x1D17, 'o'], [0x1D97, 'o'], [0x1E4D, 'o'], [0x1E4F, 'o'], [0x1E51, 'o'],
			[0x1E53, 'o'], [0x1ECD, 'o'], [0x1ECF, 'o'], [0x1ED1, 'o'], [0x1ED3, 'o'], [0x1ED5, 'o'], [0x1ED7, 'o'], [0x1ED9, 'o'], [0x1EDB, 'o'], [0x1EDD, 'o'], [0x1EDF, 'o'],
			[0x1EE1, 'o'], [0x1EE3, 'o'], [0x2092, 'o'], [0x24DE, 'o'], [0x2C7A, 'o'], [0xA74B, 'o'], [0xA74D, 'o'], [0xFF4F, 'o'], [0x152, 'OE'], [0x276, 'OE'], [0xA74E, 'OO'],
			[0x222, 'OU'], [0x1D15, 'OU'], [0x24AA, '(o)'], [0x153, 'oe'], [0x1D14, 'oe'], [0xA74F, 'oo'], [0x223, 'ou'],
			[0x1A4, 'P'], [0x1D18, 'P'], [0x1E54, 'P'], [0x1E56, 'P'], [0x24C5, 'P'], [0x2C63, 'P'], [0xA750, 'P'], [0xA752, 'P'], [0xA754, 'P'], [0xFF30, 'P'],
			[0x1A5, 'p'], [0x1D71, 'p'], [0x1D7D, 'p'], [0x1D88, 'p'], [0x1E55, 'p'], [0x1E57, 'p'], [0x24DF, 'p'], [0xA751, 'p'], [0xA753, 'p'], [0xA755, 'p'], [0xA7FC, 'p'],
			[0xFF50, 'p'], [0x24AB, '(p)'],
			[0x24A, 'Q'], [0x24C6, 'Q'], [0xA756, 'Q'], [0xA758, 'Q'], [0xFF31, 'Q'],
			[0x138, 'q'], [0x24B, 'q'], [0x2A0, 'q'], [0x24E0, 'q'], [0xA757, 'q'], [0xA759, 'q'], [0xFF51, 'q'], [0x24AC, '(q)'], [0x239, 'qp'],
			[0x154, 'R'], [0x156, 'R'], [0x158, 'R'], [0x210, 'R'], [0x212, 'R'], [0x24C, 'R'], [0x280, 'R'], [0x281, 'R'], [0x1D19, 'R'], [0x1D1A, 'R'], [0x1E58, 'R'], [0x1E5A, 'R'],
			[0x1E5C, 'R'], [0x1E5E, 'R'], [0x24C7, 'R'], [0x2C64, 'R'], [0xA75A, 'R'], [0xA782, 'R'], [0xFF32, 'R'], [0x155, 'r'], [0x157, 'r'], [0x159, 'r'], [0x211, 'r'], [0x213, 'r'],
			[0x24D, 'r'], [0x27C, 'r'], [0x27D, 'r'], [0x27E, 'r'], [0x27F, 'r'], [0x1D63, 'r'], [0x1D72, 'r'], [0x1D73, 'r'], [0x1D89, 'r'], [0x1E59, 'r'], [0x1E5B, 'r'], [0x1E5D, 'r'],
			[0x1E5F, 'r'], [0x24E1, 'r'], [0xA75B, 'r'], [0xA783, 'r'], [0xFF52, 'r'], [0x24AD, '(r)'],
			[0x15A, 'S'], [0x15C, 'S'], [0x15E, 'S'], [0x160, 'S'], [0x218, 'S'], [0x1E60, 'S'], [0x1E62, 'S'], [0x1E64, 'S'], [0x1E66, 'S'], [0x1E68, 'S'], [0x24C8, 'S'], [0xA731, 'S'],
			[0xA785, 'S'], [0xFF33, 'S'], [0x15B, 's'], [0x15D, 's'], [0x15F, 's'], [0x161, 's'], [0x17F, 's'], [0x219, 's'], [0x23F, 's'], [0x282, 's'], [0x1D74, 's'], [0x1D8A, 's'],
			[0x1E61, 's'], [0x1E63, 's'], [0x1E65, 's'], [0x1E67, 's'], [0x1E69, 's'], [0x1E9C, 's'], [0x1E9D, 's'], [0x24E2, 's'], [0xA784, 's'], [0xFF53, 's'], [0x1E9E, 'SS'],
			[0x24AE, '(s)'], [0xDF, 'ss'], [0xFB06, 'st'],
			[0x162, 'T'], [0x164, 'T'], [0x166, 'T'], [0x1AC, 'T'], [0x1AE, 'T'], [0x21A, 'T'], [0x23E, 'T'], [0x1D1B, 'T'], [0x1E6A, 'T'], [0x1E6C, 'T'], [0x1E6E, 'T'], [0x1E70, 'T'],
			[0x24C9, 'T'], [0xA786, 'T'], [0xFF34, 'T'],
			[0x163, 't'], [0x165, 't'], [0x167, 't'], [0x1AB, 't'], [0x1AD, 't'], [0x21B, 't'], [0x236, 't'], [0x287, 't'], [0x288, 't'], [0x1D75, 't'], [0x1E6B, 't'], [0x1E6D, 't'],
			[0x1E6F, 't'], [0x1E71, 't'], [0x1E97, 't'], [0x24E3, 't'], [0x2C66, 't'], [0xFF54, 't'], [0xDE, 'TH'], [0xA766, 'TH'], [0xA728, 'TZ'], [0x24AF, '(t)'], [0x2A8, 'tc'],
			[0xFE, 'th'], [0x1D7A, 'th'], [0xA767, 'th'], [0x2A6, 'ts'], [0xA729, 'tz'],
			[0xD9, 'U'], [0xDA, 'U'], [0xDB, 'U'], [0xDC, 'U'], [0x168, 'U'], [0x16A, 'U'], [0x16C, 'U'], [0x16E, 'U'], [0x170, 'U'], [0x172, 'U'], [0x1AF, 'U'], [0x1D3, 'U'], [0x1D5, 'U'],
			[0x1D7, 'U'], [0x1D9, 'U'], [0x1DB, 'U'], [0x214, 'U'], [0x216, 'U'], [0x244, 'U'], [0x1D1C, 'U'], [0x1D7E, 'U'], [0x1E72, 'U'], [0x1E74, 'U'], [0x1E76, 'U'], [0x1E78, 'U'],
			[0x1E7A, 'U'], [0x1EE4, 'U'], [0x1EE6, 'U'], [0x1EE8, 'U'], [0x1EEA, 'U'], [0x1EEC, 'U'], [0x1EEE, 'U'], [0x1EF0, 'U'], [0x24CA, 'U'], [0xFF35, 'U'],
			[0xF9, 'u'], [0xFA, 'u'], [0xFB, 'u'], [0xFC, 'u'], [0x169, 'u'], [0x16B, 'u'], [0x16D, 'u'], [0x16F, 'u'], [0x171, 'u'], [0x173, 'u'], [0x1B0, 'u'], [0x1D4, 'u'], [0x1D6, 'u'],
			[0x1D8, 'u'], [0x1DA, 'u'], [0x1DC, 'u'], [0x215, 'u'], [0x217, 'u'], [0x289, 'u'], [0x1D64, 'u'], [0x1D99, 'u'], [0x1E73, 'u'], [0x1E75, 'u'], [0x1E77, 'u'], [0x1E79, 'u'],
			[0x1E7B, 'u'], [0x1EE5, 'u'], [0x1EE7, 'u'], [0x1EE9, 'u'], [0x1EEB, 'u'], [0x1EED, 'u'], [0x1EEF, 'u'], [0x1EF1, 'u'], [0x24E4, 'u'], [0xFF55, 'u'], [0x24B0, '(u)'],
			[0x1D6B, 'ue'], [0x1B2, 'V'], [0x245, 'V'], [0x1D20, 'V'], [0x1E7C, 'V'], [0x1E7E, 'V'], [0x1EFC, 'V'], [0x24CB, 'V'], [0xA75E, 'V'], [0xA768, 'V'], [0xFF36, 'V'], [0x28B, 'v'],
			[0x28C, 'v'], [0x1D65, 'v'], [0x1D8C, 'v'], [0x1E7D, 'v'], [0x1E7F, 'v'], [0x24E5, 'v'], [0x2C71, 'v'], [0x2C74, 'v'], [0xA75F, 'v'], [0xFF56, 'v'], [0xA760, 'VY'],
			[0x24B1, '(v)'], [0xA761, 'vy'],
			[0x174, 'W'], [0x1F7, 'W'], [0x1D21, 'W'], [0x1E80, 'W'], [0x1E82, 'W'], [0x1E84, 'W'], [0x1E86, 'W'], [0x1E88, 'W'], [0x24CC, 'W'], [0x2C72, 'W'], [0xFF37, 'W'], [0x175, 'w'],
			[0x1BF, 'w'], [0x28D, 'w'], [0x1E81, 'w'], [0x1E83, 'w'], [0x1E85, 'w'], [0x1E87, 'w'], [0x1E89, 'w'], [0x1E98, 'w'], [0x24E6, 'w'], [0x2C73, 'w'], [0xFF57, 'w'], [0x24B2, '(w)'],
			[0x1E8A, 'X'], [0x1E8C, 'X'], [0x24CD, 'X'], [0xFF38, 'X'], [0x1D8D, 'x'], [0x1E8B, 'x'], [0x1E8D, 'x'], [0x2093, 'x'], [0x24E7, 'x'], [0xFF58, 'x'], [0x24B3, '(x)'],
			[0xDD, 'Y'], [0x176, 'Y'], [0x178, 'Y'], [0x1B3, 'Y'], [0x232, 'Y'], [0x24E, 'Y'], [0x28F, 'Y'], [0x1E8E, 'Y'], [0x1EF2, 'Y'], [0x1EF4, 'Y'], [0x1EF6, 'Y'], [0x1EF8, 'Y'],
			[0x1EFE, 'Y'], [0x24CE, 'Y'], [0xFF39, 'Y'],
			[0xFD, 'y'], [0xFF, 'y'], [0x177, 'y'], [0x1B4, 'y'], [0x233, 'y'], [0x24F, 'y'], [0x28E, 'y'], [0x1E8F, 'y'], [0x1E99, 'y'], [0x1EF3, 'y'], [0x1EF5, 'y'], [0x1EF7, 'y'],
			[0x1EF9, 'y'], [0x1EFF, 'y'], [0x24E8, 'y'], [0xFF59, 'y'], [0x24B4, '(y)'],
			[0x179, 'Z'], [0x17B, 'Z'], [0x17D, 'Z'], [0x1B5, 'Z'], [0x21C, 'Z'], [0x224, 'Z'], [0x1D22, 'Z'], [0x1E90, 'Z'], [0x1E92, 'Z'], [0x1E94, 'Z'], [0x24CF, 'Z'], [0x2C6B, 'Z'],
			[0xA762, 'Z'], [0xFF3A, 'Z'],
			[0x17A, 'z'], [0x17C, 'z'], [0x17E, 'z'], [0x1B6, 'z'], [0x21D, 'z'], [0x225, 'z'], [0x240, 'z'], [0x290, 'z'], [0x291, 'z'], [0x1D76, 'z'], [0x1D8E, 'z'], [0x1E91, 'z'],
			[0x1E93, 'z'], [0x1E95, 'z'], [0x24E9, 'z'], [0x2C6C, 'z'], [0xA763, 'z'], [0xFF5A, 'z'], [0x24B5, '(z)'],
			[0x2070, '0'], [0x2080, '0'], [0x24EA, '0'], [0x24FF, '0'], [0xFF10, '0'],
			[0xB9, '1'], [0x2081, '1'], [0x2460, '1'], [0x24F5, '1'], [0x2776, '1'], [0x2780, '1'], [0x278A, '1'], [0xFF11, '1'], [0x2488, '1.'], [0x2474, '(1)'],
			[0xB2, '2'], [0x2082, '2'], [0x2461, '2'], [0x24F6, '2'], [0x2777, '2'], [0x2781, '2'], [0x278B, '2'], [0xFF12, '2'], [0x2489, '2.'], [0x2475, '(2)'],
			[0xB3, '3'], [0x2083, '3'], [0x2462, '3'], [0x24F7, '3'], [0x2778, '3'], [0x2782, '3'], [0x278C, '3'], [0xFF13, '3'], [0x248A, '3.'], [0x2476, '(3)'],
			[0x2074, '4'], [0x2084, '4'], [0x2463, '4'], [0x24F8, '4'], [0x2779, '4'], [0x2783, '4'], [0x278D, '4'], [0xFF14, '4'], [0x248B, '4.'], [0x2477, '(4)'],
			[0x2075, '5'], [0x2085, '5'], [0x2464, '5'], [0x24F9, '5'], [0x277A, '5'], [0x2784, '5'], [0x278E, '5'], [0xFF15, '5'], [0x248C, '5.'], [0x2478, '(5)'],
			[0x2076, '6'], [0x2086, '6'], [0x2465, '6'], [0x24FA, '6'], [0x277B, '6'], [0x2785, '6'], [0x278F, '6'], [0xFF16, '6'], [0x248D, '6.'], [0x2479, '(6)'],
			[0x2077, '7'], [0x2087, '7'], [0x2466, '7'], [0x24FB, '7'], [0x277C, '7'], [0x2786, '7'], [0x2790, '7'], [0xFF17, '7'], [0x248E, '7.'], [0x247A, '(7)'],
			[0x2078, '8'], [0x2088, '8'], [0x2467, '8'], [0x24FC, '8'], [0x277D, '8'], [0x2787, '8'], [0x2791, '8'], [0xFF18, '8'], [0x248F, '8.'], [0x247B, '(8)'],
			[0x2079, '9'], [0x2089, '9'], [0x2468, '9'], [0x24FD, '9'], [0x277E, '9'], [0x2788, '9'], [0x2792, '9'], [0xFF19, '9'], [0x2490, '9.'], [0x247C, '(9)'],
			[0x2469, '10'], [0x24FE, '10'], [0x277F, '10'], [0x2789, '10'], [0x2793, '10'], [0x2491, '10.'], [0x247D, '(10)'],
			[0x246A, '11'], [0x24EB, '11'], [0x2492, '11.'], [0x247E, '(11)'], [0x246B, '12'], [0x24EC, '12'], [0x2493, '12.'], [0x247F, '(12)'],
			[0x246C, '13'], [0x24ED, '13'], [0x2494, '13.'], [0x2480, '(13)'], [0x246D, '14'], [0x24EE, '14'], [0x2495, '14.'], [0x2481, '(14)'],
			[0x246E, '15'], [0x24EF, '15'], [0x2496, '15.'], [0x2482, '(15)'], [0x246F, '16'], [0x24F0, '16'], [0x2497, '16.'], [0x2483, '(16)'],
			[0x2470, '17'], [0x24F1, '17'], [0x2498, '17.'], [0x2484, '(17)'], [0x2471, '18'], [0x24F2, '18'], [0x2499, '18.'], [0x2485, '(18)'],
			[0x2472, '19'], [0x24F3, '19'], [0x249A, '19.'], [0x2486, '(19)'], [0x2473, '20'], [0x24F4, '20'], [0x249B, '20.'], [0x2487, '(20)'],
			[0xAB, '"'], [0xBB, '"'], [0x201C, '"'], [0x201D, '"'], [0x201E, '"'], [0x2033, '"'], [0x2036, '"'], [0x275D, '"'], [0x275E, '"'], [0x276E, '"'], [0x276F, '"'], [0xFF02, '"'],
			[0x2018, '\''], [0x2019, '\''], [0x201A, '\''], [0x201B, '\''], [0x2032, '\''], [0x2035, '\''], [0x2039, '\''], [0x203A, '\''], [0x275B, '\''], [0x275C, '\''], [0xFF07, '\''],
			[0x2010, '-'], [0x2011, '-'], [0x2012, '-'], [0x2013, '-'], [0x2014, '-'], [0x207B, '-'], [0x208B, '-'], [0xFF0D, '-'], [0x2045, '['], [0x2772, '['], [0xFF3B, '['], [0x2046, ']'],
			[0x2773, ']'], [0xFF3D, ']'], [0x207D, '('], [0x208D, '('], [0x2768, '('], [0x276A, '('], [0xFF08, '('], [0x2E28, '(('], [0x207E, ')'], [0x208E, ')'], [0x2769, ')'], [0x276B, ')'],
			[0xFF09, ')'], [0x2E29, '))'], [0x276C, '<'], [0x2770, '<'], [0xFF1C, '<'], [0x276D, '>'], [0x2771, '>'], [0xFF1E, '>'], [0x2774, '{'], [0xFF5B, '{'], [0x2775, '}'], [0xFF5D, '}'],
			[0x207A, '+'], [0x208A, '+'], [0xFF0B, '+'], [0x207C, '='], [0x208C, '='], [0xFF1D, '='], [0xFF01, '!'], [0x203C, '!!'], [0x2049, '!?'], [0xFF03, '#'], [0xFF04, '$'], [0x2052, '%'],
			[0xFF05, '%'], [0xFF06, '&'], [0x204E, '*'], [0xFF0A, '*'], [0xFF0C, ','], [0xFF0E, '.'], [0x2044, '/'], [0xFF0F, '/'], [0xFF1A, ':'], [0x204F, ';'], [0xFF1B, ';'], [0xFF1F, '?'],
			[0x2047, '??'], [0x2048, '?!'], [0xFF20, '@'], [0xFF3C, '\\'], [0x2038, '^'], [0xFF3E, '^'], [0xFF3F, '_'], [0x2053, '~'], [0xFF5E, '~']
		]);
		/* eslint-enable no-magic-numbers, max-len */

		let result = '';
		for (let i = 0, len = str.length; i < len; i++) {
			let c = str.charAt(i);
			let char_code = c.charCodeAt(0);

			if (char_code > 127) { // eslint-disable-line no-magic-numbers
				let ascii = map.get(char_code);
				if (ascii) {
					result += ascii;
				}
				else {
					result += '_';
				}
			}
			else {
				result += c;
			}
		}

		return result;
	} // utf82ascii()

	/**
	 * Formats a string as a slug, e.g. "Hello World" ==> "hello-world"
	 * - Note: all non-alphanumeral characters will be removed
	 * - Note: output is a lower-case ASCII string, does not begin/end with dash, has no consecutive dashes
	 * @param {string} n
	 * @return {string}
	 */
	function slugify(n) {
		if (typeof n !== 'string') {
			throw new TypeError('slugify() - param is not str: ' + n);
		}

		return utf82ascii(n)
		.toLowerCase()
		.replace(/\s+/g, '-')         // Replace spaces with dashes
		.replace(/[^\w-]+/g, '')      // Remove all non-word chars
		.replace(/-+/g, '-')          // Collapse consecutive dashes
		.trim()                       // Trim spaces
		.replace(/^[-]+|[-]+$/g, ''); // Trim dashes
	} // slugify()

	/**
	 * Parses a CSV string into a 2D array of strings.
	 * Conforms to RFC-4180 (https://tools.ietf.org/html/rfc4180), which allows multiple records (each a CSV)
	 * @param {string} data
	 * @param {Object} opts (optional)
	 *                  - {string} delimiter (defaults to comma. Must be at least 1 char)
	 *                  - {string} quote {defaults to ". Must be at least 1 char)
	 *                  - {string} lineEnding (defaults to \r\n. Must be at least 1 char)
	 *                  - {boolean} parseHeader (defaults to true, i.e. 1st row is returned as an array at index 0.)
	 *                  - {boolean} trim (defaults to true)
	 * @return {Array} 2D array of strings
	 */
	function csv2array(data, opts) {
		if (typeof data !== 'string') {
			throw new TypeError('csv2array() - data is not str: ' + data);
		}

		let options = processCSVOptions(opts);

		// Cache lengths
		let quote_len = options['quote'].length;
		let delimiter_len = options['delimiter'].length;
		let line_ending_len = options['lineEnding'].length;

		let result = [ [''] ];
		let row = 0;
		let col = 0;
		let inside_enclosure = false;
		let expect_delimiter_or_eof = false;

		// Go through each character
		let i = 0;
		let len = data.length; // tmp variables for loops
		while (i < len) {
			if (data.substr(i, quote_len) === options['quote']) {
				if (inside_enclosure) {
					if (data.substr(i + quote_len, quote_len) === options['quote']) {
						result[row][col] += options['quote'];
						i += quote_len;
					} // escaped quote
					else {
						inside_enclosure = false;
						expect_delimiter_or_eof = true;
					} // terminate enclosure
				} // inside enclosure
				else {
					if (result[row][col].length > 0) {
						throw new TypeError('csv2array() - invalid csv. Unexpected quote at char ' + i + ' (row: ' + row + ', col: ' + col);
					} // check that no characters appear before/after quotes
					else {
						inside_enclosure = true;
					} // start enclosure
				} // not in enclosure

				i += quote_len;
			} // Case: quote
			else if (data.substr(i, delimiter_len) === options['delimiter']) {
				if (inside_enclosure) {
					result[row][col] += options['delimiter'];
				} // inside enclosure
				else {
					if (options['trim']) {
						result[row][col] = String(result[row][col]).trim();
					}

					// Go to next word
					col++;
					result[row][col] = '';
					expect_delimiter_or_eof = false;
				} // not in enclosure

				i += delimiter_len;
			} // Case: delimiter
			else if (data.substr(i, line_ending_len) === options['lineEnding']) {
				if (inside_enclosure) {
					result[row][col] += options['lineEnding'];
				} // inside enclosure
				else {
					if (options['trim']) {
						result[row][col] = String(result[row][col]).trim();
					}
					row++;
					col = 0;
					result[row] = [''];
					expect_delimiter_or_eof = false;
				} // not in enclosure

				i += line_ending_len;
			} // Case: line ending
			else {
				if (expect_delimiter_or_eof) {
					throw new TypeError('csv2array() - invalid csv. Expecting delimiter at char ' + i + ' (row: ' + row + ', col: ' + col);
				}
				result[row][col] += data.charAt(i);

				i++;
			} // Case: regular char
		} // while there are more chars

		// check that quotes are closed
		if (inside_enclosure) {
			throw new TypeError('csv2array() - data is not valid csv. Unclosed quote detected.');
		}

		// Last item may not have been trimmed
		if (options['trim']) {
			result[row][col] = String(result[row][col]).trim();
		}

		if (options['parseHeader']) {
			// Check that each row contains the same number of fields
			for (i = 1; i < result.length; i++) { // note: cannot cache length
				if (result[i].length !== result[0].length) {
					result.splice(i, 1);
					i--; // compensate index due to splice
				}
			}
		} // header is present
		else {
			result.shift();
		} // no headers necessary

		return result;
	} // csv2array()

	/**
	 * Parse a delimited string into an array of strings.
	 * Useful for quick parsing of strings into array
	 * - Note: an empty string input will return an empty array
	 * - Note: does not fully conform to RFC-4180 (https://tools.ietf.org/html/rfc4180), which allows multiple records
	 *         this function only parses a CSV and returns a one-dimensional array, i.e. only works on one record
	 * @param {string} data
	 *        {Object} opts (optional)
	 *                  - {string} delimiter (defaults to comma. Must be at least 1 char)
	 *                  - {string} quote (defaults to ". Must be at least 1 char)
	 *                  - {boolean} trim (defaults to true. Operates on individual result)
	 * @return {Array} of strings
	 */
	function csvRecord2array(data, opts) {
		if (typeof data !== 'string') {
			throw new TypeError('csvRecord2array() - data is not str');
		}

		let options = processCSVOptions(opts);

		// Cache lengths
		let quote_len = options['quote'].length;
		let delimiter_len = options['delimiter'].length;

		let result = []; // result to return
		let value = ''; // current value parsed so far
		let inside_enclosure = false;
		let expect_delimiter_or_eof = false; // after exiting quotes, we only expect delimiter or EOF

		// Go through each character
		let i = 0;
		let len = data.length; // tmp variables for loops
		while (i < len) {
			if (data.substr(i, quote_len) === options['quote']) {
				if (inside_enclosure) {
					if (data.substr(i + quote_len, quote_len) === options['quote']) {
						value += options['quote'];
						i += quote_len;
					} // escaped quote
					else {
						inside_enclosure = false;
						expect_delimiter_or_eof = true;
					} // terminate enclosure
				} // inside enclosure
				else {
					if (value.length > 0) {
						throw new TypeError('csvRecord2array() - data is not valid csv. Quotes cannot appear here: char ' + (i + 1));
					} // check that no characters appear before/after quotes
					else {
						inside_enclosure = true;
					} // start enclosure
				} // not in enclosure

				i += quote_len;
			} // Case: quote
			else if (data.substr(i, delimiter_len) === options['delimiter']) {
				if (inside_enclosure) {
					value += options['delimiter'];
				} // inside enclosure
				else {
					// push this value into result array
					if (options['trim']) {
						result.push(value.trim());
					}
					else {
						result.push(value);
					}

					// reset to original state
					value = '';
					expect_delimiter_or_eof = false;
				} // not in enclosure

				i += delimiter_len;
			} // Case: delimiter
			else {
				let c = data.charAt(i);
				if (expect_delimiter_or_eof) {
					if (!options['trim'] || c !== ' ') {
						throw new TypeError('csvRecord2array() - data is not csv. Unexpected data after double-quotes at char ' + (i + 1));
					}
				}
				value += c;

				i++;
			} // Case: regular char
		} // while we still have chars

		// check that quotes are closed
		if (inside_enclosure) {
			throw new TypeError('csvRecord2array() - data is not valid csv. Unclosed quote detected.');
		}

		// push last value into result array
		if (options['trim']) {
			value = value.trim();
		}
		result.push(value);

		return result;
	} // csvRecord2array()

	/**
	 * Renders a template by replacing all {{ }} and {{{ }}} occurrences with actual data
	 * - Note: Any template string enclosed in double curly braces {{  }} will be automatically escaped.
	 * - Note: Any template string enclosed in triple curly braces {{{  }}} will NOT be escaped.
	 * - Note: Nested variables won't work, e.g. {{some-{{hello}}-variable-name}}
	 * - Note: data will be auto-casted to strings
	 * - Note: Template strings cannot contain any unclosed double or triple braces.
	 * @param {string} template
	 * @param {Object} data - key-value pairs
	 * @return {string}
	 */
	function renderTemplate(template, data) {
		if (typeof template !== 'string') {
			throw new TypeError('renderTemplate() - template is not a string.');
		}
		if (typeof data !== 'object') {
			throw new TypeError('renderTemplate() - data is not an object.');
		}

		let section_stack = [{
			'name': '',
			'data': data,
			'result': '',
			'toRender': true
		}];
		let context_section = section_stack[0]; // always the last item of array

		// Iterate the template char by char
		let i = 0;                   // iterator
		let len = template.length;
		let section_name, section_data; // loop vars
		while (i < len) {
			let c = template.charAt(i);

			// Check for opening braces
			if (c !== '{' || (i + 1) >= len || template[i + 1] !== '{') {
				if (context_section['toRender']) {
					context_section['result'] += c;
				}

				i++;
				continue;
			} // regular character

			// Look for closing brace
			let closing_tag_index = template.indexOf('}}', i + 1);
			if (closing_tag_index === -1) {
				throw new Error('renderTemplate() - unclosed tag at ' + (i + 1));
			}

			// Get name of this tag
			let tag_name = template.substring(i + 2, closing_tag_index); // eslint-disable-line no-magic-numbers
			if (tag_name.startsWith('!')) {
				i = closing_tag_index + 2; // eslint-disable-line no-magic-numbers
			}
			else if (tag_name.startsWith('#')) {
				section_name = tag_name.substr(1);
				section_data = getContextData(section_name);

				// Figure if we need to render/loop this section
				let loop_count = 0;
				let to_render = Boolean(section_data);
				if (Array.isArray(section_data)) {
					if (section_data.length === 0) {
						to_render = false;
					} // empty array - don't render section
					else {
						to_render = true;
						loop_count = section_data.length - 1;
					} // non-empty array - render section as a loop
				} // section's data is Array

				// Use this section as context
				let section = {
					'name': section_name,
					'data': section_data,
					'result': '',
					'toRender': to_render,
					'loopIndex': i + 4 + tag_name.length,  // eslint-disable-line no-magic-numbers
					'loopCount': loop_count,
					'loopedCount': 0
				};
				// console.debug('Entering new section: ' + section_name); console.debug(section);
				section_stack.push(section);
				context_section = section;

				// Advance iterator
				i += 4 + tag_name.length; // eslint-disable-line no-magic-numbers
			} // found section start
			else if (tag_name.startsWith('/')) {
				section_name = tag_name.substr(1);
				// console.debug('Found closing: ' + section_name);

				// Error cases
				if (context_section['name'] === '') {
					throw new Error('renderTemplate() - unopened section ' + section_name + ' at ' + i);
				} // we are not in a section
				if (context_section['name'] !== section_name) {
					throw new Error('renderTemplate() - unclosed section ' + context_section['name'] + ' at ' + i);
				}

				// Check if we need to loop
				if (context_section['loopedCount'] < context_section['loopCount']) {
					// console.debug('Looping section: ' + context_section['name']); console.debug(context_section);
					context_section['loopedCount']++;
					i = context_section['loopIndex'];
				}
				else {
					// Exit section
					// console.debug('Exiting section: ' + context_section['name']); console.debug(context_section);
					let old_section = section_stack.pop();
					context_section = section_stack[section_stack.length - 1];
					// console.debug('Current section: ' + context_section['name']); console.debug(context_section);

					// Pass result to
					if (context_section['toRender'] && old_section['result'] !== '') {
						context_section['result'] += old_section['result'];
					}

					// Advance iterator
					i += 4 + tag_name.length; // eslint-disable-line no-magic-numbers
				}
			} // found section end
			else if (tag_name.startsWith('{')) {
				if ((closing_tag_index + 1) >= len || template.charAt(closing_tag_index + 2) !== '}') { // eslint-disable-line no-magic-numbers
					throw new Error('renderTemplate() - unclosed tag at ' + (i + 1));
				}

				tag_name = tag_name.substr(1);

				if (context_section['toRender']) {
					context_section['result'] += castToString(getContextData(tag_name));
				}

				// Advance iterator
				i += 6 + tag_name.length; // eslint-disable-line no-magic-numbers
			} // found triple brace variable
			else {
				if (context_section['toRender']) {
					context_section['result'] += escapeHTML(castToString(getContextData(tag_name)));
				}

				// Advance iterator
				i += 4 + tag_name.length; // eslint-disable-line no-magic-numbers
			} // found double brace variable
		} // while loop

		// Ensure we are back outside of all sections
		if (context_section['name'] !== '') {
			throw new Error('renderTemplate() - unclosed section "' + context_section['name'] + '" at ' + i);
		}

		return context_section['result'];

		/**
		 * Casts to string, keeping numbers and booleans
		 * @param {?} x
		 * @return {string}
		 */
		function castToString(x) {
			let type = typeof x;
			if (type === 'string') {
				return x;
			}
			if (type === 'number' || type === 'boolean') {
				return String(x);
			}
			if (type === 'object' && x !== null) {
				return x.toString();
			}

			return '';
		} // castToString()

		/**
		 * Find the value of a variable in the current context
		 * @param {string} var_name
		 * @return {?}
		 */
		function getContextData(var_name) {
			// loop variables
			let stack_size = section_stack.length;
			let j = stack_size - 1;

			while (j >= 0) {
				let curr_section = section_stack[j--]; // remember to decrement j
				// console.debug(var_name + ' at ' + curr_section['name'] + ': ' + curr_section['data'][var_name]);

				if (typeof curr_section['data'] !== 'object') {
					continue;
				}

				// Determine correct data to use (depending on loop or not)
				let data_to_use;
				if (Array.isArray(curr_section['data'])) {
					if (curr_section['loopedCount'] <= curr_section['loopCount']) {
						data_to_use = curr_section['data'][curr_section['loopedCount']];
					} // still looping
					else {
						return '';
					}
				} // looped section
				else {
					data_to_use = curr_section['data'];
					// console.debug('Found ' + var_name + ' in ' + curr_section['name']);
					// console.debug(curr_section['data'][var_name]);
				} // normal section

				// Return
				if (var_name === '.') {
					// console.debug(data_to_use, Array.isArray(curr_section['data']));
					return '' + data_to_use;
				} // simple loop items
				else if (data_to_use.hasOwnProperty(var_name)) {
					return data_to_use[var_name];
				}
			} // while there are still more sections in the stack

			// console.debug('Data "' + var_name + '" not found');
			return '';
		} // getContextData()
	} // renderTemplate()

	/**
	 * Formats string as a file name compatible with all known OSes
	 * - Note: all non-alphanumeral, non-underscore, non-dot characters will be converted into dashes
	 * - Note: output is a lower-case ASCII string, does not begin/end with dash, has no consecutive dashes
	 * @param {string} n
	 * @return {string}
	 */
	function encodeFileName(n) {
		if (typeof n !== 'string') {
			throw new TypeError('encodeFileName() - param is not str: ' + n);
		}

		return utf82ascii(n)
		.replace(/[^\w.-]/g, '-')               // Convert non-alphanumeral, non-underscore, non-dot to dashes
		.replace(/-+/g, '-')                    // Collapse consecutive dashes
		.replace(/^[-]+|[-]+$/g, '')            // Trim dashes
		.toLowerCase();
	} // encodeFileName()

	/**
	 * Same as encodeFileName(), but preserves slashes, e.g. "/var/www/hello/world"
	 * - Note: only works with forward slash /, does not work with backslash \
	 * @param {string}
	 * @return {string}
	 */
	function encodeFilePath(n) {
		if (typeof n !== 'string') {
			throw new TypeError('encodeFilePath() - param is not str');
		}

		let parts = n.split('/').map(function(part) {
			return encodeFileName(part);
		});

		return parts.join('/');
	} // encodeFilePath()

	/**
	 * Encodes a string into URL-safe base64 string (RFC-4648)
	 * - JavaScript's built-in btoa() does not encode UTF-8 strings
	 * - Note: if a one-char string is specified as padding, it will be used
	 * @param {string} str
	 * @param {boolean | string} padding - whether to pad result  (DEFAULT: false)
	 * @return {string}
	 */
	function encodeBase64(str, padding) {
		if (typeof str !== 'string') {
			throw new TypeError('encodeBase64() - param is not str: ' + str);
		}

		// Transform
		let result = encodeURIComponent(str);
		result = result.replace(/%([0-9A-F]{2})/g, function(match, p1) {
			return String.fromCharCode('0x' + p1);
		});
		result = btoa(result);
		result = result.replace(/[+/]/g, function(c) {
			if (c === '+') {
				return '-';
			}
			if (c === '/') {
				return '_';
			}
			return '';
		});

		// Pad
		result = trimEndBy(result, '='); // pad char used by btoa()
		/* eslint-disable no-magic-numbers */
		if (padding) {
			let pad_len = 4 - (result.length % 4);
			if (pad_len === 4) {
				pad_len = 0;
			}
			let pad_char = padding ? String(padding) : '';
			if (pad_char.length !== 1) {
				pad_char = '='; // DEFAULT PAD CHAR
			}
			result = result.padEnd(result.length + pad_len, pad_char);
		}
		/* eslint-enable no-magic-numbers */

		return result;
	} // encodeBase64()

	/**
	 * Decodes a base64 ASCII string into original form (binary)
	 * - JavaScript's built-in atob() does not decode to UTF-8 strings
	 * - Opposite of encodeBase64
	 * @param {string} str
	 * @return {string}
	 */
	function decodeBase64(str) {
		if (typeof str !== 'string') {
			throw new TypeError('decodeBase64() - param is not str: ' + str);
		}

		// Pad
		/* eslint-disable no-magic-numbers */
		let pad_len = 4 - (str.length % 4);
		if (pad_len === 4) {
			pad_len = 0;
		}
		let pad_char = '='; // used by atob()
		let padded_str = str.padEnd(str.length + pad_len, pad_char);
		/* eslint-enable no-magic-numbers */

		// Transform
		padded_str = padded_str.replace(/[_-]/g, function(c) {
			if (c === '-') {
				return '+';
			}
			if (c === '_') {
				return '/';
			}
			return '';
		});

		return decodeURIComponent(atob(padded_str).split('')
		.map(function(c) {
			return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2); // eslint-disable-line no-magic-numbers
		})
		.join(''));
	} // decodeBase64()

	/**
	 * Trims a string by a certain character, i.e. removes from beginning and ending of string
	 * - Note: if 2nd param is array, only strings will be used; non-string elements will be ignored
	 * @param {string} s - original string
	 * @param {string | Array} c - the string to trim away, or array of strings
	 * @return {string}
	 */
	function trimBy(s, c) {
		return trimEndBy(trimStartBy(s, c), c);
	} // trimBy()
	function trimStartBy(s, c) {
		if (typeof s !== 'string') {
			throw new TypeError('trimStartBy() - 1st param is not str');
		}
		let chars = [];
		if (typeof c === 'string') {
			chars.push(c);
		}
		else if (typeof c === 'object' && c instanceof Array) {
			c.forEach(function(x) {
				if (typeof x === 'string') {
					if (chars.includes(x)) {
						return;
					}
					chars.push(x);
				}
			});

			// Sort by descending length (so we operate on longer match sequences first)
			chars.sort(function(a, b) {
				return b.length - a.length || // sort by length, if equal then
				       a.localeCompare(b);    // sort by dictionary order
			});
		}
		else {
			throw new TypeError('trimStartBy() - invalid 2nd param: ' + c);
		}

		// Trivial case: nothing to do
		if (chars.length === 1 && chars[0] === '') {
			return s;
		}

		let str = s;

		let i, len; // loop vars

		// Trim front
		let done = false;
		while (!done) {
			done = true;

			for (i = 0, len = chars.length; i < len; i++) {
				if (str.startsWith(chars[i])) {
					str = str.substring(chars[i].length);
					done = false;
				}
			}
		}

		return str;
	} // trimStartBy()
	function trimEndBy(s, c) {
		if (typeof s !== 'string') {
			throw new TypeError('trimEndBy() - 1st param is not str');
		}
		let chars = [];
		if (typeof c === 'string') {
			chars.push(c);
		}
		else if (typeof c === 'object' && c instanceof Array) {
			c.forEach(function(x) {
				if (typeof x === 'string') {
					if (chars.includes(x)) {
						return;
					}
					chars.push(x);
				}
			});

			// Sort by descending length (so we operate on longer match sequences first)
			chars.sort(function(a, b) {
				return b.length - a.length || // sort by length, if equal then
				       a.localeCompare(b);    // sort by dictionary order
			});
		}
		else {
			throw new TypeError('trimEndBy() - invalid 2nd param: ' + c);
		}

		// Trivial case: nothing to do
		if (chars.length === 1 && chars[0] === '') {
			return s;
		}

		let str = s;

		let i, len; // loop vars

		// Trim back
		let done = false;
		while (!done) {
			done = true;

			for (i = 0, len = chars.length; i < len; i++) {
				if (str.endsWith(chars[i])) {
					str = str.substring(0, str.length - chars[i].length);
					done = false;
				}
			}
		}

		return str;
	} // trimEndBy()

	/**
	 * Counts the occurrences of a string within another string
	 * - Note: if needle is '' (empty), returns haystack length + 1
	 * @param {string} haystack
	 * @param {string} needle
	 * @param {Object=} options (optional)
	 *            {number} offset - where to start counting
	 *            {number} length - how many chars to count (negative lengths count backwards)
	 *            {boolean} allowOverlap (DEFAULT: false)
	 * @return {number} an integer of count
	 */
	function count(haystack, needle, opts) {
		// Data checks
		if (typeof haystack !== 'string') {
			throw new TypeError('count() - 1st param is not str');
		}
		if (typeof needle !== 'string') {
			throw new TypeError('count() - 2nd param is not str');
		}

		// Trivial case
		if (needle.length <= 0) {
			return haystack.length + 1;
		}

		// Check Options
		let DEFAULT_OPTIONS = {
			'offset': 0,
			'length': haystack.length,
			'allowOverlap': false
		};
		let options = Object.assign({}, DEFAULT_OPTIONS, opts);
		if (typeof options['allowOverlap'] !== 'boolean') {
			options['allowOverlap'] = false;
		}
		if (typeof options['offset'] !== 'number') {
			options['offset'] = 0;
		}
		if (typeof options['length'] !== 'number') {
			options['length'] = haystack.length;
		}

		// Process the haystack
		let counter = 0;
		let str = haystack.substring(options['offset'], options['offset'] + options['length']);
		let step = options['allowOverlap'] ? 1 : needle.length;
		let pos = 0;
		while (true) {
			pos = str.indexOf(needle, pos);

			if (pos >= 0) {
				counter++;
				pos += step;
			}
			else {
				break;
			}
		}

		return counter;
	} // count()

	/**
	 * Calculates Levenshtein distance between two strings.
	 * @param {string} str1
	 * @param {string} str2
	 * @return {number} integer
	 */
	function levenshtein(str1, str2) {
		if (typeof str1 !== 'string') {
			throw new TypeError('levenshtein() - str1 is not string');
		}
		if (typeof str2 !== 'string') {
			throw new TypeError('levenshtein() - str2 is not string');
		}

		// Trivial cases
		if (str1 === str2) {
			return 0;
		}
		let str1_len = str1.length;
		let str2_len = str2.length;
		if (str1_len === 0) {
			return str2_len;
		}
		if (str2_len === 0) {
			return str1_len;
		}

		let i, j, tmp; // loop vars

		// initialise previous row
		let prev_row = [];
		for (i = 0; i <= str2_len; i++) {
			prev_row[i] = i;
		}

		// Calculate current row distance from previous row
		let next_col;
		for (i = 0; i < str1_len; i++) {
			next_col = i + 1;

			// go through each char of str2
			for (j = 0; j < str2_len; j++) {
				let cur_col = next_col;

				// substitution
				next_col = prev_row[j] + (str1.charAt(i) === str2.charAt(j) ? 0 : 1);

				// insertion
				tmp = cur_col + 1;
				if (next_col > tmp) {
					next_col = tmp;
				}

				// deletion
				tmp = prev_row[j + 1] + 1;
				if (next_col > tmp) {
					next_col = tmp;
				}

				// copy cur_col value into previous (in preparation for next iteration)
				prev_row[j] = cur_col;
			}

			// copy last col value into previous (in preparation for next iteration)
			prev_row[j] = next_col;
		}

		return next_col;
	} // levenshtein()

	/**
	 * Gets the first letter of every word
	 * - Note: if a string is shorter than min length, an error will be thrown.
	 * - Note: if a string has fewer words than min length, letters from the first words will be used.
	 * - Note: if a string has more words than max length, the first and last words will be used.
	 * @param {string} str
	 * @param {Object=} options (optional)
	 *             - {string} delimiter - default: ' ' (SPACE)
	 *             - {int} minLength - defualt: 0 (no min)
	 *             - {int} maxLength - defualt: str.length (no max)
	 * @return {string}
	 */
	function initials(str, opts) {
		if (typeof str !== 'string') {
			throw new TypeError('initials() - param is not a string: ' + str);
		}

		// Check Options
		let options = Object.assign({}, opts);
		if (typeof options !== 'object' || options === null) {
			options = {
				'delimiter': ' '
			};
		}
		if (typeof options['delimiter'] !== 'string') {
			options['delimiter'] = ' ';
		}
		let words = str.split(options['delimiter']);
		let letter_count = str.length - words.length + 1;

		if (typeof options['maxLength'] !== 'number' || options['maxLength'] > letter_count) {
			options['maxLength'] = letter_count;
		}
		if (typeof options['minLength'] !== 'number' || options['minLength'] < 0) {
			options['minLength'] = 0;
		}
		if (options['minLength'] > letter_count) {
			throw new RangeError('initials() - not enough letters to produce minLength ' + options['minLength'] + ' for: ' + str);
		}
		if (options['minLength'] > options['maxLength']) {
			throw new RangeError('initials() - minLength (' + options['minLength'] + ') exceeds maxLength (' + options['maxLength'] + ')');
		}

		// Trivial case
		if (str === '') {
			return '';
		}

		// Prepare indices
		let front_word_index = 0; // index to run loop from front to back
		let back_word_index = words.length - 1; // index to run loop from back to front
		let take_from_front = false; // toggling flag
		let front_initials = words[0].charAt(0);
		let back_initials = '';

		// Check if we need more than just the first letters
		let additional_count = options['minLength'] - words.length;
		let curr_word_length = words[0].length;
		let curr_char_index = 1;
		while (additional_count > 0) {
			if (curr_char_index === curr_word_length) {
				front_word_index++;
				additional_count++; // remember to increment for each word we go past
				curr_word_length = words[front_word_index].length;
				curr_char_index = 0;
			} // word has no more letters

			front_initials = front_initials + words[front_word_index].charAt(curr_char_index);
			additional_count--;
			curr_char_index++;
		}
		// console.log('DEBUG: front initials: ' + front_initials );
		if (front_initials !== '') {
			front_word_index++; // remember to move on since we have already "consumed" this word
		}

		// Find initials until we hit maxLength
		while (front_word_index <= back_word_index && front_initials.length + back_initials.length < options['maxLength']) {
			if (take_from_front) {
				front_initials = front_initials + words[front_word_index].charAt(0);
				front_word_index++;
			}
			else {
				back_initials = words[back_word_index].charAt(0) + back_initials;
				back_word_index--;
			}
			take_from_front = !take_from_front;
		} // while

		return front_initials + back_initials;
	} // initials()

	/**
	 * Returns a randomly generated version 4 UUID.
	 * @return {string}
	 */
	function uuidv4() {
		/* eslint-disable */
		// Use Math.random if window.crypto is not supported
		if (!global['crypto'].getRandomValues) {
			return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
				let r = Math.random() * 16 | 0,
					v = c == 'x' ? r : r & 0x3 | 0x8;
				return v.toString(16);
			});
		}

		return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, function(c) {
			return (c ^ global['crypto'].getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16);
		});
		/* eslint-enable */
	} // uuidv4()

	/**
	 * Checks if a string / number contains an integer representation, e.g. "-43", "0", 15", "98.0"
	 * - Note: empty strings evaluate to false
	 * @param {string | number} str
	 * @return {boolean}
	 */
	function isInt(str) {
		if (typeof str === 'number') {
			return str % 1 === 0;
		}
		if (typeof str === 'string') {
			if (str.trim() === '') {
				return false;
			}

			return str % 1 === 0;
		}

		return false;
	} // isInt()

	/**
	 * Checks if a string / number contains an unsigned integer, e.g. "0", "17", "12.0"
	 * - Note: empty strings evaluate to false
	 * @param {string | number} str
	 * @return {boolean}
	 */
	function isUInt(str) {
		if (typeof str === 'number') {
			return str % 1 === 0 && str >= 0;
		}
		if (typeof str === 'string') {
			if (str.trim() === '') {
				return false;
			}

			return str % 1 === 0 && str >= 0;
		}

		return false;
	} // isUInt()

	/**
	 * Checks if a string/number is a number (floats / integers)
	 * @param {string | number} str
	 * @return {boolean}
	 */
	function isNumber(str) {
		if (typeof str !== 'number' && typeof str !== 'string') {
			return false;
		}

		return !isNaN(parseFloat(str)) && isFinite(str);

		// jQuery
		// return (str - parseFloat(str) + 1) >= 0;
	} // isNumber()

	/**
	 * Checks if string contains only ASCII printable characters
	 * @param {string} str
	 * @return {boolean}
	 */
	function isASCII(str) {
		if (typeof str !== 'string') {
			return false;
		}

		if (str === '') {
			return true;
		}

		let ascii = /^[\x00-\x7F]+$/; /* eslint-disable-line no-control-regex */
		return ascii.test(str);
	} // isASCII()

	/**
	 * Checks if a string is a valid email on the internet (domain name with TLD)
	 * - Note: does not work with non-ASCII, non-letter TLDs
	 * - Note: not 100% compliant with any spec. It uses sensible rules, e.g.:
	 *     TRUE for "test@example.co"
	 *     FALSE for "test@localhost"
	 *     FALSE for "test@102.254.7.18"
	 * @param {string} str
	 * @return {boolean}
	 */
	function isEmail(str) {
		if (typeof str !== 'string') {
			return false;
		}


		let re = new RegExp('^' +
			// Local part
			'(' +
				'(' +
					'[^<>()[\\]\\.,;:\\s@"]+' +       // first char
					'(\\.[^<>()[\\]\\.,;:\\s@"]+)*' + // subsequent chars
				')' +
				'|' +
				'(".+")' + // dots in quotess
			')' +
			'@' +
			// Domain part
			'(' +
				// IP address in square brackets
				'(\\[' +
					'([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.' +
					'([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.' +
					'([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.' +
					'([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])' +
				'\\])' +
				'|' +
				// Domain name
				'(' +
					'([a-zA-Z\\-0-9]+\\.)+' + // letters, digits, hyphens
					'[a-zA-Z]{2,63}' +
				')' +
			')' +
		'$');

		return re.test(str);
	} // isEmail()

	/**
	 * Checks if a string is a valid URL on the www, e.g. TRUE for "http://www.example.com", FALSE for "http://localhost"
	 * - Copyright (c) 2010-2013 Diego Perini (https://gist.github.com/dperini/729294)
	 * @param {string} str
	 * @return {boolean}
	 */
	function isURL(str) {
		if (typeof str !== 'string') {
			return false;
		}

		let pattern = new RegExp('^' +
			'(?:(?:https?|ftp)://)' +  // protocol identifier
			'(?:\\S+(?::\\S*)?@)?' +   // user:pass authentication
			'(?:' +
				'localhost' +
			'|' +
				// IP address dotted notation octets (excludes 0.0.0.0, >= 224.0.0.0, network and broadcast addresses)
				// uncomment the next few lines to exclude private & local networks
				// '(?!(?:10|127)(?:\\.\\d{1,3}){3})' +
				// '(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})' +
				// '(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})' +
				'(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])' +
				'(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}' +
				'(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))' +
			'|' +
				// host name
				'(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)' +
				// domain name
				'(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*' +
				// TLD identifier
				'(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))' +
				// TLD may end with dot
				'.?' +
			')' +
			// port number
			'(?::\\d{2,5})?' +
			// resource path
			'(?:[/?#]\\S*)?' +
			'$', 'i');

		return pattern.test(str);
	} // isURL()

	/**
	 * Checks if a string is a valid UUID (v4)
	 * @param {string} str
	 * @return {boolean}
	 */
	function isUUID(str) {
		if (typeof str !== 'string') {
			return false;
		}

		return (/^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i).test(str);
	} // isUUID()


/*********************************************
* Collections / Objects                      *
*  - expects param to be Set, Array,         *
*    HTMLCollecton etc.                      *
*  - also works with object literals         *
**********************************************/
	/**
	 * Creates a shuffled copy of an Array / Set / string
	 * @param {Array | Set | string} obj
	 * @return {Array | Set | string}
	 */
	function shuffle(obj) {
		let type = typeof obj;
		if (type !== 'object' && type !== 'string') {
			throw new TypeError('shuffle() - cannot process: ' + type);
		}
		if (obj === null) {
			throw new TypeError('shuffle() - cannot process null.');
		}

		// Create an array of the original object (we'll be removing elems from it)
		let operated;
		if (type === 'string') {
			operated = obj.split('');
		}
		else if (obj instanceof Array) {
			operated = obj.slice();
		}
		else if (obj instanceof Set) {
			operated = Array.from(obj);
		}
		else {
			throw new TypeError('shuffle() - can only process Array / Set.');
		}

		let result = [];
		while (operated.length > 0) {
			let i = Math.floor(Math.random() * operated.length);
			result.push(operated[i]);
			operated.splice(i, 1); // note: modifies array in place
		}

		if (type === 'string') {
			return result.join('');
		}
		if (obj instanceof Array) {
			return result;
		}

		let set = new Set();
		result.forEach(function(v) {
			set.add(v);
		});
		return set;
	} // shuffle()

	/**
	 * Computes union of multiple Sets (or array-like collections)
	 * - Primarily meant for Sets. All other objects, e.g. HTMLCollection, will return as Array
	 * - Note: all duplicate values are removed
	 * - Note: non-array-like objects are treated as empty arrays
	 * @param {object, object, ...object}
	 * @return {Set | Array}
	 */
	function union() {
		// Check args
		let len = arguments.length;
		if (len === 0 || len === 1) {
			throw new TypeError('union() expects at least 2 params');
		}
		if (typeof arguments[0] !== 'object' || arguments[0] === null) {
			throw new TypeError('union() expects 1st param to be a non-null object');
		}
		if (typeof arguments[1] !== 'object' || arguments[1] === null) {
			throw new TypeError('union() expects 2nd param to be a non-null object');
		}

		let result = new Set(Array.from(arguments[0]));
		// result = obj2set(arguments[0]);

		// Add each argument's items into result Set
		for (let i = 1; i < len; i++) {
			let arg = arguments[i];
			if (typeof arg === 'undefined' || arg === null) {
				throw new TypeError('union() expects non-null arguments. arg ' + i + ' is null/undefined.');
			}

			if (!(arg instanceof Set)) {
				arg = Array.from(arg);
			}

			arg.forEach(addToResult);
		}
		function addToResult(v) {
			result.add(v);
		}

		// Return either Set or Array
		return arguments[0] instanceof Set ? result : Array.from(result);
	} // union()

	/**
	 * Computes intersection of multiple Sets (or array-like collections)
	 * - primarily meant for Set
	 * - all other objects, e.g. HTMLCollection, will return as Array
	 * - Note: non-array-like objects are treated as empty arrays
	 * - Note: elements are compared by reference, so {'a':1} !== {'a':1}
	 * @param {object, object, ...object}
	 * @return {Set | Array}
	 */
	function intersection() {
		// Check args
		let len = arguments.length;
		if (len === 0 || len === 1) {
			throw new TypeError('intersection() expects at least 2 params');
		}
		if (typeof arguments[0] !== 'object' || arguments[0] === null) {
			throw new TypeError('intersection() expects 1st param to be a non-null object');
		}
		if (typeof arguments[1] !== 'object' || arguments[1] === null) {
			throw new TypeError('intersection() expects 2nd param to be a non-null object');
		}

		// Edge case: only 2 args
		if (len === 2) { // eslint-disable-line no-magic-numbers
			// Convert args to sets
			let tmp0 = new Set(Array.from(arguments[0]));
			let tmp1 = new Set(Array.from(arguments[1]));

			let result = new Set();

			// Add common elements to result
			tmp0.forEach(function(v) {
				if (tmp1.has(v)) {
					result.add(v);
				}
			});

			// Return either Set or Array
			return arguments[0] instanceof Set ? result : Array.from(result);
		}

		// More than 2 args
		let result = new Set(Array.from(arguments[0]));
		// result = obj2set(arguments[0]);

		for (let i = 1; i < len; i++) {
			if (typeof arguments[i] === 'undefined' || arguments[i] === null) {
				throw new TypeError('intersection() expects non-null iterable arguments. arg ' + i + ' is null/undefined.');
			}
			result = intersection(result, arguments[i]);
		}

		// Return either Set or Array
		return arguments[0] instanceof Set ? result : Array.from(result);
	} // intersection()

	/**
	 * Computes difference of multiple Sets (or array-like collections)
	 * - all other objects, e.g. HTMLCollection, will return as Array
	 * - Note: non-array-like objects are treated as empty arrays
	 * - Note: elements are compared by reference, so {'a':1} !== {'a':1}
	 * @param {object, object, ...object}
	 * @return {Set | Array}
	 */
	function difference() {
		// Check args
		let len = arguments.length;
		if (len === 0 || len === 1) {
			throw new TypeError('difference() expects at least 2 params');
		}
		if (typeof arguments[0] !== 'object' || arguments[0] === null) {
			throw new TypeError('difference() expects 1st param to be a non-null object');
		}
		if (typeof arguments[1] !== 'object' || arguments[1] === null) {
			throw new TypeError('difference() expects 2nd param to be a non-null object');
		}

		// Edge case: only 2 args
		if (len === 2) { // eslint-disable-line no-magic-numbers
			let result = new Set();

			// Convert args to sets
			let tmp0 = new Set(Array.from(arguments[0]));
			let tmp1 = new Set(Array.from(arguments[1]));
			// tmp0 = obj2set(arguments[0]);
			// tmp1 = obj2set(arguments[1]);

			tmp0.forEach(function(v) {
				if (!tmp1.has(v)) {
					result.add(v);
				}
			});

			// Return either Set or Array
			return arguments[0] instanceof Set ? result : Array.from(result);
		}

		// More than 2 args
		let result = new Set(Array.from(arguments[0]));
		for (let i = 1; i < len; i++) {
			if (typeof arguments[i] === 'undefined' || arguments[i] === null) {
				throw new TypeError('difference() expects non-null iterable arguments. arg ' + i + ' is null/undefined.');
			}
			result = difference(result, arguments[i]);
		}

		// Return either Set or Array
		return arguments[0] instanceof Set ? result : Array.from(result);
	} // difference()

	/**
	 * Extends a Map with other Maps, or an object with other objects.
	 * Multiple objects may be passed in (non-objects are ignored).
	 * - Note: the object in the first argument will be extended and returned.
	 * - Note: values in later objects overwrite earlier objects.
	 * - Note: when extending objects, all keys must be strings, else toString() will be automatically applied by JS engine
	 * @param {object, object, ...object}
	 * @return {Map | object}
	 */
	function extend() {
		// Check args
		let len = arguments.length;
		if (len === 0 || len === 1) {
			throw new TypeError('extend() expects at least 2 params');
		}
		if (typeof arguments[0] !== 'object' || arguments[0] === null) {
			throw new TypeError('extend() expects 1st param to be a non-null object');
		}

		// Extend arguments[0] with the other objects
		let obj = arguments[0];
		for (let i = 1; i < len; i++) {
			let arg = arguments[i];

			// ignore non-objects
			if (typeof arg !== 'object') {
				continue;
			}

			if (arg instanceof Map || arg instanceof Set || arg instanceof Array) {
				if (obj instanceof Map) {
					arg.forEach(extendMap);
				} // extending Map with Map/Array
				else {
					arg.forEach(extendObj);
				} // extending generic object with Map/Array
			} // arg is Map / Array
			else {
				for (let key in arg) {
					if (!arg.hasOwnProperty(key)) {
						continue;
					}

					if (obj instanceof Map) {
						obj.set(key, arg[key]);
					} // extending Map with obj prop
					else {
						obj[key] = arg[key];
					} // extending generic object with object
				}
			} // arg is generic object
		}

		function extendMap(v, k) {
			obj.set(k, v);
		}
		function extendObj(v, k) {
			obj[k] = v;
		}

		return obj;
	} // extend()

	/**
	 * @param {Object} obj
	 * @return {Map}
	 */
	function obj2map(obj) {
		if (typeof obj !== 'object' || obj === null) {
			throw new TypeError('obj2map() - expects non-null objects');
		}
		return extend(new Map(), obj);
	} // obj2map()

	/**
	 * @param {Object} obj
	 * @return {Set}
	 */
	function obj2set(obj) {
		if (typeof obj !== 'object' || obj === null) {
			throw new TypeError('obj2set() - expects non-null objects');
		}

		return new Set(obj2array(obj));
	} // obj2set()

	/**
	 * Returns an array from an object
	 * @param {Object} obj
	 * @return {Array}
	 */
	function obj2array(obj) {
		if (typeof obj !== 'object' || obj === null) {
			throw new TypeError('obj2array() - expects non-null objects');
		}
		if (obj instanceof Array) {
			return obj.slice();
		}
		if (obj instanceof Set) {
			return Array.from(obj);
		}
		if (obj instanceof Map) {
			return Array.from(obj);
		}
		if (obj.hasOwnProperty('length')) { // Array-like objects
			return Array.from(obj);
		}

		return Array.from(obj2map(obj));
	} // obj2array()

	/**
	 * @private
	 * Called by multiple CSV parsing functions
	 * @param {Object} options
	 * @return {Object}
	 */
	function processCSVOptions(opts) {
		let DEFAULT_OPTIONS = {
			'delimiter': ',',
			'lineEnding': '\r\n',
			'quote': '"',
			'trim': true,
			'parseHeader': true
		};
		let options = Object.assign({}, DEFAULT_OPTIONS, opts);

		// Process options
		if (typeof options['delimiter'] !== 'string' || options['delimiter'].length < 1) {
			options['delimiter'] = ',';
		}
		if (typeof options['lineEnding'] !== 'string' || options['lineEnding'].length < 1) {
			options['lineEnding'] = '\r\n';
		}
		if (typeof options['quote'] !== 'string' || options['quote'].length < 1) {
			options['quote'] = '"';
		}
		if (typeof options['trim'] !== 'boolean') {
			options['trim'] = true;
		}
		if (typeof options['parseHeader'] !== 'boolean') {
			options['parseHeader'] = true;
		}

		return options;
	} // processCSVOptions()

	/**
	 * Converts object to a CSV string
	 * - Note: if called using Array, Set or Map, will redirect to the appropriate functions.
	 * - Note: toString() will be called on all properties / elements, with null values replaced by string "null"
	 * @param {Object | Array | Set | Map} obj - can be object literal, Array, Set, Map
	 *        {Object} options (optional)
	 *                  - {string} delimiter (defaults to comma. Must be at least 1 char)
	 *                  - {string} lineEnding (defaults to \r\n. Must be 1 or 2 chars)
	 *                  - {string} quote (defaults to ". Must be at least 1 char)
	 *                  - {boolean} trim (defaults to true)
	 *                  - {boolean} parseHeader (defaults to true)
	 * @return {string}
	 */
	function obj2csv(obj, options) {
		// Return base on type
		if (typeof obj !== 'object' || obj === null) {
			throw new TypeError('obj2csv() - expects non-null objects');
		}
		else if (obj instanceof Array) {
			return array2csv(obj, options);
		}
		else if (obj instanceof Set) {
			return set2csv(obj, options);
		}
		else if (obj instanceof Map) {
			return map2csv(obj, options);
		}
		else {
			return objLiteral2csv(obj, options);
		}
	} // obj2csv()

	/**
	 * Converts an array into a RFC-4180 compliant CSV string.
	 * - Note: if array is NOT a 2D array with all 2nd-dimension arrays of same length, redirect to array2csvRecord()
	 * - Note: toString() will be called on all properties / elements, with null values replaced by string "null"
	 * @param {Array} arr - 2D array
	 *        {Object} options (optional)
	 *                  - {string} delimiter (defaults to comma. Must be at least 1 char)
	 *                  - {string} lineEnding (defaults to \r\n. Must be 1 or 2 chars)
	 *                  - {string} quote (defaults to ". Must be at least 1 char)
	 *                  - {boolean} trim (defaults to true)
	 *                  - {boolean} parseHeader (defaults to true)
	 * @return {string}
	 */
	function array2csv(arr, opts) {
		let len = arr.length;
		if (len === 0) {
			return '';
		}

		let options = processCSVOptions(opts);

		// Check if we are dealing with a 2D array
		let is_multi_array = true;
		for (let i = 0; i < len; i++) {
			if (!(arr[i] instanceof Array) || arr[i].length !== arr[0].length || arr[i].length === 0) {
				is_multi_array = false;
				break;
			} // each item must be an array, and have the exact same length.
		}
		if (!is_multi_array) {
			return array2csvRecord(arr, options);
		}

		let result = '';
		if (options['parseHeader']) {
			result += array2csvRecord(arr[0], options) + options['lineEnding'];
		}

		for (let i = 1; i < len; i++) {
			result += array2csvRecord(arr[i], options) + options['lineEnding'];
		}

		return result.slice(0, -options['lineEnding'].length);
	} // array2csv()

	/**
	 * Converts an array into a CSV-record string (one line)
	 * - Note: toString() will be called on all properties / elements, with null values replaced by string "null"
	 * @param {Array} arr - 2D array
	 *        {Object} options (optional)
	 *                  - {string} delimiter (defaults to comma. Must be at least 1 char
	 *                  - {string} lineEnding (defaults to \r\n. Must be 1 or 2 chars)
	 *                  - {string} quote (defaults to ". Must be at least 1 char)
	 *                  - {boolean} trim (defaults to true)
	 * @return {string}
	 */
	function array2csvRecord(arr, opts) {
		let options = processCSVOptions(opts);

		// regex for quote detection (to escape quotes)
		let regex = new RegExp(options['quote'], 'g');

		let curr; // loop vars
		let result = '';
		for (let i = 0, len = arr.length; i < len; i++) {
			// get a string value of current item
			curr = arr[i];
			if (curr === null) {
				curr = 'null';
			}
			else if (typeof curr === 'object') {
				curr = JSON.stringify(curr);
			}
			else if (typeof curr === 'undefined') {
				curr = '';
			}
			else {
				curr = curr.toString();
			}

			// add item to result
			if (curr.includes(options['delimiter']) || curr.includes(options['quote']) || curr.includes(options['lineEnding'])) {
				result +=
					options['quote'] +
					curr.replace(regex, options['quote'] + options['quote']) + // escape quotes
					options['quote'] + options['delimiter'];
			} // values containing line endings, quotes and commas must be enclosed
			else {
				result += curr + options['delimiter'];
			}
		}

		if (result) {
			return result.slice(0, -options['delimiter'].length); // remove last delimiter
		}

		return result;
	} // array2csvRecord()

	/**
	 * Converts a set into a CSV string
	 * - Note: toString() will be called on all properties / elements
	 * @param {Set} obj
	 *        {Object} options (optional)
	 *                  - {string} delimiter (defaults to comma. Must be at least 1 char)
	 *                  - {string} lineEnding (defaults to \r\n. Must be 1 or 2 chars)
	 *                  - {string} quote (defaults to ". Must be at least 1 char)
	 *                  - {boolean} trim (defaults to true)
	 *                  - {boolean} parseHeader (defaults to true)
	 * @return {string}
	 */
	function set2csv(obj, options) {
		let arr = Array.from(obj);
		return array2csv(arr, options);
	} // set2csv()

	/**
	 * Converts a map to a CSV string
	 * - Note: toString() will be called on all properties / elements, with null values replaced by string "null"
	 * @param {Map} obj
	 *        {Object} options (optional)
	 *                  - {string} delimiter (defaults to comma. Must be at least 1 char)
	 *                  - {string} lineEnding (defaults to \r\n. Must be 1 or 2 chars)
	 *                  - {string} quote (defaults to ". Must be at least 1 char)
	 *                  - {boolean} trim (defaults to true)
	 *                  - {boolean} parseHeader (defaults to true)
	 * @return {string}
	 */
	function map2csv(obj, opts) {
		let options = processCSVOptions(opts);

		// Note: cannot simply convert map to array, which produces elems of length === 2. We want keys to be an array
		let keys = [];
		let values = [];
		obj.forEach(function(v, k) {
			keys.push(k);
			values.push(v);
		});
		return array2csv([keys, values], options);
	} // map2csv()

	/**
	 * @private
	 */
	function objLiteral2csv(obj, opts) {
		let options = processCSVOptions(opts);

		let key, curr; // loop vars

		// regex for quote detection (to escape quotes)
		let regex = new RegExp(options['quote'], 'g');

		let result = '';

		// Parse Header
		if (options['parseHeader']) {
			for (key in obj) {
				if (!obj.hasOwnProperty(key)) {
					continue;
				}

				// get string value of current item
				curr = key.toString(); // note: keys can be strings / numbers

				// add item to result
				if (curr.includes(options['delimiter']) || curr.includes(options['quote']) || curr.includes(options['lineEnding'])) {
					result +=
						options['quote'] +
						curr.replace(regex, options['quote'] + options['quote']) + // escape quotes
						options['quote'] + options['delimiter'];
				} // values containing line endings, quotes and commas must be enclosed
				else {
					result += curr + options['delimiter'];
				}
			}

			if (result) {
				result = result.slice(0, -options['delimiter'].length) + options['lineEnding']; // remove last delimiter, add line ending
			}
		} // parse header

		// Parse properties
		for (key in obj) {
			if (!obj.hasOwnProperty(key)) {
				continue;
			}

			// get string value of current item
			curr = obj[key];
			if (curr === null) {
				curr = 'null';
			}
			else if (typeof curr === 'object') {
				curr = JSON.stringify(curr);
			}
			else {
				curr = curr.toString();
			}

			// add item to result
			if (curr.includes(options['delimiter']) || curr.includes(options['quote']) || curr.includes(options['lineEnding'])) {
				result +=
					options['quote'] +
					curr.replace(regex, options['quote'] + options['quote']) + // escape quotes
					options['quote'] + options['delimiter'];
			} // values containing line endings, quotes and commas must be enclosed
			else {
				result += curr + options['delimiter'];
			}
		} // for

		if (result) {
			result = result.slice(0, -options['delimiter'].length); // remove last delimiter
		}

		return result;
	} // objLiteral2csv()

	// Export
	global['eve'] = {

		/* String Utils */
		'escapeHTML': escapeHTML,
		'htmlspecialchars': escapeHTML,
		'unescapeHTML': unescapeHTML,
		'htmlspecialchars_decode': unescapeHTML,
		'encodeURL': encodeURL,
		'decodeURL': decodeURL,
		'encodeURLComponent': encodeURLComponent,
		'decodeURLComponent': decodeURLComponent,
		'br2nl': br2nl,
		'nl2br': nl2br,
		'utf82ascii': utf82ascii,
		'slugify': slugify,
		'csvRecord2array': csvRecord2array,
		'csv2array': csv2array,
		'encodeFileName': encodeFileName,
		'encodeFilePath': encodeFilePath,
		'renderTemplate': renderTemplate,
		'encodeBase64': encodeBase64,
		'decodeBase64': decodeBase64,
		'trimBy': trimBy,
		'trimStartBy': trimStartBy,
		'trimLeftBy': trimStartBy,
		'trimEndBy': trimEndBy,
		'trimRightBy': trimEndBy,
		'count': count,
		'levenshtein': levenshtein,
		'initials': initials,
		'getIntials': initials,
		'uuidv4': uuidv4,
		'uuid': uuidv4,
		'isInt': isInt,
		'isUInt': isUInt,
		'isUnsignedInt': isUInt,
		'isNumber': isNumber,
		'isNumeric': isNumber,
		'isASCII': isASCII,
		'isEmail': isEmail,
		'isURL': isURL,
		'isUUID': isUUID,

		/* Collection / Object Utils */
		'shuffle': shuffle,
		'union': union,
		'intersection': intersection,
		'difference': difference,
		'extend': extend,
		'obj2map': obj2map,
		'obj2set': obj2set,
		'obj2array': obj2array,
		'obj2csv': obj2csv,
		'array2csv': array2csv,
		'array2csvRecord': array2csvRecord,
		'arrayRecord2csv': array2csvRecord,
		'set2csv': set2csv,
		'map2csv': map2csv
	};
}(this));
